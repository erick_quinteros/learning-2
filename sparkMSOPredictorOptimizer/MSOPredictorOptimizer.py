# Update python path to find DataAccessLayer
import os
import sys

# Update sys path to find the modules from Common and DataAccessLayer
script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
learning_dir = os.path.dirname(script_dir)
sys.path.append(learning_dir)

import logging
from common.DataAccessLayer.SparkDataAccessLayer import SparkLearningAccessor, CommonDbAccessor, initialize
#from common.DataAccessLayer.DebugSparkDataAccessLayer import SparkLearningAccessor, CommonDbAccessor, initialize
from common.DataAccessLayer.DataAccessLayer import LearningAccessor
from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
from common.logger.Logger import create_logger
from common.DataAccessLayer.DataAccessLayer import DSEAccessor
import datetime
from h2o.estimators import H2ORandomForestEstimator
from pysparkling import *
from pyspark.sql import SparkSession


TOP_N_PREDICTORS = 40
IMPORTANCE_THRESHOLD = 0.01
VALUE_SEP_CHAR = ";"


# Global variable
logger = None
spark = None

account_df = None
account_product_df_map = {}
product_goal_df_map = {}

class MSOPredictorOptimizer:
    """
    This class is responsible for generating and using optimal predictors for MSO
    """

    learning_home_dir = None

    def __init__(self, learning_home_dir):
        """

        :param learning_home_dir:
        """
        self.learning_home_dir = learning_home_dir
        self.__intialize_logger()

    def __get_mso_predictor_logger_name(self):
        """

        :return:
        """
        return "MSO_PREDICTOR_OPTIMIZER"

    def __intialize_logger(self):
        """

        :return:
        """
        # Create simulation logger path
        timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        log_file_path = self.learning_home_dir + "/../logs/msopredictoroptimizer." + timestamp + ".stdout"

        # Crete logger with file and output
        global logger
        logger_name = self.__get_mso_predictor_logger_name()
        logger = create_logger(logger_name, log_file_path, logging.DEBUG)
        logger.debug("Logger initialization completed.")

    def __get_channel_list(self):
        """
        This function return the list of available channels for the current installation
        :return:
        """
        return ['SEND_CHANNEL']

    def __get_goal_list(self):
        """
        This function returns the list of goals
        :return:
        """
        return ['OPEN', 'CLICK']

    def __get_product_list(self):
        """
        This function returns the list of product external Ids which are active and not deleted in the database
        :return:
        """
        learning_accessor = LearningAccessor()
        product_uids = learning_accessor.get_enabled_products_for_learning()

        return product_uids

    def get_the_design_matrix(self, product_uid, goal, hc):
        """
        This will return the design matrix for h2o model for the specified product.
        :param product_uid: external id of the product
        :param goal: goal for which processing the design matrix
        :return:
        """
        logger.debug("Started preparing the design matrix for for product={PRODUCT} and goal={GOAL}.."
                     .format(PRODUCT=product_uid, GOAL=goal))
        global account_df
        global account_product_df_map
        global product_goal_df_map

        timestamp_col_labels = ['createdAt', 'updatedAt']

        # Initialize DataAccessLayer
        learning_accessor = SparkLearningAccessor()

        # Get Account data from db
        if not account_df:
            logger.debug("Fetching Account data frame from the database..")
            account_df = learning_accessor.get_account_table()
            account_df = account_df.drop(*timestamp_col_labels)
            account_df.cache()

        # Get Account-Product from db for specific product
        if product_uid in account_product_df_map:
            account_product_df = account_product_df_map[product_uid]
        else:
            logger.debug("Fetching AccountProduct data frame for Product={PRODUCT} from the database.."
                         .format(PRODUCT=product_uid))
            account_product_df = learning_accessor.get_account_product_table(product_uid)
            account_product_df = account_product_df.drop(*timestamp_col_labels)
            account_product_df.cache()
            account_product_df_map[product_uid] = account_product_df

        # Get the target (goal) data from events for the product
        product_goal_str = product_uid + goal
        if product_goal_str in product_goal_df_map:
            account_event_df = product_goal_df_map[product_goal_str]
        else:
            logger.debug("Fetching Event data frame for Product={PRODUCT} and Goal={GOAL} from the database.."
                         .format(PRODUCT=product_uid, GOAL=goal))
            account_event_df = learning_accessor.get_target_column(product_uid, goal)
            account_event_df.cache()
            product_goal_df_map[product_goal_str] = account_event_df

        # Merge Account, Account-Product, target
        logger.debug("Merging Account and Account Product data frames..")
        training_df = account_df.join(account_product_df, on='accountId', how='left')

        logger.debug("Merging Account and Event data frames..")
        training_df = training_df.join(account_event_df, on='accountId',  how='left')

        # Drop Un-necessary cols
        logger.debug("Dropping unnecessary columns..")
        col_labels_to_remove = ['accountName', 'externalId', 'isDeleted']
        training_df = training_df.drop(*col_labels_to_remove)

        training_df = training_df.fillna(0)

        # Convert the training spark dataframe as H2O frame
        logger.debug("Converting design matrix to H2O frame..")
        training_h2o_df = hc.as_h2o_frame(training_df)

        # Update the string columns to factors and convert float to int
        logger.debug("Getting the string type column list from H2O frame..")
        string_col_idx = training_h2o_df.columns_by_type(coltype="string")
        string_col_idx = [round(x) for x in string_col_idx]

        # Convert the string cols in the factor
        logger.debug("Convert the string type columns to factors in H2O frame..")
        training_h2o_df[:, string_col_idx] = training_h2o_df[:, string_col_idx].asfactor()

        # Convert necessary integer cols into factor
        logger.debug("Convert the necessary columns to factors in H2O frame..")
        training_h2o_df['facilityId'] = training_h2o_df['facilityId'].asfactor()
        training_h2o_df['productId'] = training_h2o_df['productId'].asfactor()

        logger.debug("Completed preparing the design matrix for for product={PRODUCT} and goal={GOAL}.."
                     .format(PRODUCT=product_uid, GOAL=goal))

        return training_h2o_df

    def write_variable_importance(self, variable_importance_df, goal, channel, product_uid):
        """

        :param variable_importance:
        :return:
        """
        spark_learning_accessor = SparkLearningAccessor()
        spark_learning_accessor.write_variable_importance(variable_importance_df, goal, channel, product_uid)

    def get_top_n_predictors(self, variable_importance_df, n=TOP_N_PREDICTORS, importance_threshold=IMPORTANCE_THRESHOLD):
        """

        :param variable_importance_df:
        :param n:
        :return:
        """
        filtered_variable_importance_df = variable_importance_df[variable_importance_df['scaled_importance'] > importance_threshold]
        top_predictors = filtered_variable_importance_df.nlargest(n, 'scaled_importance')
        top_predictors = top_predictors['variable'].tolist()

        return top_predictors

    def write_optimal_learning_params(self, top_predictor_str, goal, channel, product_uid):
        """

        :param top_predictor_str:
        :param goal:
        :param channel:
        :param product_uid:
        :return:
        """
        spark_learning_accessor = SparkLearningAccessor()
        spark_learning_accessor.write_optimal_learning_params(top_predictor_str, goal, channel, product_uid)

    def clear_database_tables(self):
        """

        :return:
        """
        learning_accessor = LearningAccessor()
        learning_accessor.clear_mso_optimal_predictor_tables()

    def generate_optimal_predictors(self):
        """
        This function generates optimal predictors for all possible MSO combination
        :return:
        """
        logger.info("Started the optimal parameter generation")

        global account_product_df_map
        global product_goal_df_map

        # Create H2O context
        hc = H2OContext.getOrCreate(spark)

        logger.debug("Fetching necessary information...")
        # 1. Get list of Channels, Goals and Products
        channels = self.__get_channel_list()
        goals = self.__get_goal_list()
        product_uids = self.__get_product_list()
        logger.debug("Active Products={PRODUCTS} \nGoals={GOALS}".format(PRODUCTS=product_uids, GOALS=goals))

        TARGET_COL = 'TARGET'
        COLS_EXLCLUDE = ['accountId', 'externalId']

        logger.debug("Clearing the database tables...")
        # Clear the existing entries in the database
        self.clear_database_tables()

        logger.debug("Started product iteration")
        # 2. Iterate through the unique combinations of goal, product and channel
        for product_uid in product_uids:
            for goal in goals:
                for channel in channels:
                    try:

                        logger.debug("Processing for product={PRODUCT}, channel={CHANNEL}, goal={GOAL}..".
                                     format(PRODUCT=product_uid, CHANNEL=channel, GOAL=goal))

                        logger.debug("Fetching the design matrix..")
                        # 2.1 Get the design matrix with account and account-product table
                        training_df = self.get_the_design_matrix(product_uid, goal, hc)

                        # 2.2 Train a DRF H2O model
                        logger.debug("Initializing the model..")
                        # Define model
                        model = H2ORandomForestEstimator(ntrees=100, max_depth=10, nfolds=10)

                        logger.debug("Training the model..")
                        # Train model
                        model.train(y=TARGET_COL, training_frame=training_df)

                        logger.debug("Getting the variable importance..")
                        # 2.3 Get the variable importance for the combination
                        varible_importance_df = model.varimp(use_pandas=True)
                        varible_importance_df = varible_importance_df[varible_importance_df.variable != 'accountId']
                        varible_importance_df = varible_importance_df[varible_importance_df.variable != 'productId']


                        logger.debug("Writing variable importance..")
                        # 2.4 Write results in the database table
                        self.write_variable_importance(varible_importance_df, goal, channel, product_uid)

                        logger.debug("Getting top-k predictors..")
                        # 2.5 Get the top-k predictors
                        top_predictors = self.get_top_n_predictors(varible_importance_df)
                        top_predictor_str = VALUE_SEP_CHAR.join(top_predictors)

                        logger.debug("Writing the optimal parameters..")
                        # 2.6 Write the optimal parameters
                        self.write_optimal_learning_params(top_predictor_str, goal, channel, product_uid)

                        # Delete the data frame
                        training_df.delete()
                        training_df.remove()

                    except Exception as e:

                        message = "Unable to generate optimal predictors for Goal='{GOAL}' Channel='{CHANNEL}' " \
                                  "and Product='{PRODUCT}' combination"
                        message = message.format(GOAL=goal, PRODUCT=product_uid, CHANNEL=channel)
                        print(message)
                        print(e)

                    logger.debug("Completed process for product={PRODUCT}, channel={CHANNEL}, goal={GOAL}.".
                                 format(PRODUCT=product_uid, CHANNEL=channel, GOAL=goal))

                # Completed goal remove goal-product dataframe
                logger.debug("Deleting the product goal dataframe..")
                product_goal_str = product_uid + goal
                del product_goal_df_map[product_goal_str]
                product_goal_df_map[product_goal_str] = None

            logger.debug("Deleting AcccountProduct dataframe for Product={PRODUCT}".format(PRODUCT=product_uid))
            # Done with the product remove product dataframe
            del account_product_df_map[product_uid]
            account_product_df_map[product_uid] = None


def get_copy_storm_database_name(dse_db_name):
    """
    This function returns the copy storm database name based on the logic and customer exceptions.
    :param dse_db_name:
    :return:
    """
    CS_DB_SUFFIX = "_cs"
    cs_db_name =  dse_db_name + CS_DB_SUFFIX

    if cs_db_name[:8] == "pfizerus":
        cs_db_name = 'pfizerprod_cs'

    return cs_db_name

def main():
    """
    This is main method and entry point for MSO Predictor Optimizer
    :return:
    """
    global spark

    # Validate the input argument to the script
    if len(sys.argv) != 7:
        logger.error("Invalid arguments to the optimizer script")

    # Retrieve the arguments
    db_host = sys.argv[1]
    db_user = sys.argv[2]
    db_password = sys.argv[3]
    dse_db_name = sys.argv[4]
    db_port = sys.argv[5]  # Update to 33066 for testing on Local machine
    learning_home_dir = sys.argv[6]

    LEARNING_DB_SUFFIX = "_learning"

    # Create Learning database name
    learning_db_name = dse_db_name + LEARNING_DB_SUFFIX
    cs_db_name = get_copy_storm_database_name(dse_db_name)

    # Get the singleton instance of the database config and set the properties
    database_config = DatabaseConfig.instance()
    database_config.set_config(db_host, db_user, db_password, db_port, dse_db_name, learning_db_name, cs_db_name)

    # Create spark context
    spark = SparkSession.builder.appName("MSOPredictorOptimizer").getOrCreate()

    # Initialize logger and SparkSession for DataAccessLayer
    initialize("MSOPredictorOptimizerLogger", spark)

    mso_predictor_optimizer = MSOPredictorOptimizer(learning_home_dir)
    mso_predictor_optimizer.generate_optimal_predictors()

if __name__ == "__main__":
    main()
