context('testing loadMessageSequenceData() func in spark MSO module')
print(Sys.time())

# writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
requiredMockDataList <- list(pfizerusdev=c('Product','RepTeam','Message','MessageSetMessage','MessageSet','Event','EventType','Interaction','ProductInteractionType','InteractionProduct','InteractionAccount','InteractionProductMessage','AccountProduct','Rep','RepTeamRep','RepAccountAssignment','Account'),pfizerusdev_learning=c('AKT_Message_Topic_Email_Learned','Sent_Email_vod__c_arc'),pfizerprod_cs=c('Approved_Document_vod__c'))
resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)

# source script and code setup
# required to run tests
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/utilsFuncSpark.r',homedir))

# set required args
productUID <- readModuleConfig(homedir, 'messageSequence','productUID')
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
con_cs <- getDBConnectionCS(dbuser, dbpassword, dbhost, dbname, port)

# spark setting
library(sparkLearning)
library(arrow)
sc <- initializeSpark(homedir)
sparkDBconURL <- sparkGetDBConnection(dbuser, dbpassword, dbhost, dbname, port)
sparkDBconURL_l <- sparkGetDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)

# run script
source(sprintf("%s/sparkMessageSequence/loadMessageSequenceData.R",homedir))
dta <- loadMessageSequenceData(sc, sparkDBconURL, sparkDBconURL_l, con,con_l,con_cs,productUID)
names(dta) <- paste(names(dta),"_new",sep="")
for(i in 1:length(dta))assign(names(dta)[i],dta[[i]])
# interactions <- dta[[1]]
# accountProduct <- dta[[2]]
# products <- dta[[3]]
# messageSet <- dta[[4]]
# emailTopicNames <- dta[[5]]
# messages <- dta[[6]]
# messageTopic <- dta[[7]]
# interactionsP <- dta[[8]]
# expiredMessages <- dta[[9]]
# accountPredictorNames <- dta[[10]]
# emailTopicNameMap <- dta[[11]]

# disconnect db
dbDisconnect(con)
dbDisconnect(con_cs)
dbDisconnect(con_l)

# test case
test_that("test have correct length of data", {
  expect_equal(length(dta), 12)
  expect_equal(dim(products_new),c(1,2))
  expect_equal(dim(messageSetMessage_new),c(10,2))
  expect_equal(dim(messageSet_new),c(4,2))
  expect_equal(length(emailTopicNames_new),29)
  expect_equal(dim(messages_new),c(446,6))
  expect_equal(dim(messageTopic_new),c(85,2))
  expect_length(expiredMessages_new,65)
  expect_length(accountPredictorNames_new,39)
  expect_length(emailTopicNameMap_new,18)
})

test_that("test have correct length of data for return spark dataframe", {
  expect_equal(sdf_dim(interactions_new),c(2099,8))
  expect_equal(sdf_dim(accountProduct_new),c(309,236))
  expect_equal(sdf_dim(interactionsP_new),c(16592,8))
})

rm(dta)
test_that("test result is the same as the one saved ", {
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_products.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messageSetMessage.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messageSet.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_emailTopicNames.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messages.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messageTopic.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_expiredMessages.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_accountPredictorNames.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_emailTopicNameMap.RData', homedir))
  expect_equal(products_new,products)
  expect_equal(messageSetMessage_new,messageSetMessage)
  expect_equal(messageSet_new,messageSet)
  expect_equal(emailTopicNames_new,emailTopicNames)
  expect_equal(messages_new[order(messageId)],messages[order(messageId)])
  expect_equal(messageTopic_new[order(physicalMessageUID)],messageTopic[order(physicalMessageUID),])
  expect_equal(expiredMessages_new,expiredMessages)
  expect_equal(accountPredictorNames_new,accountPredictorNames)
  expect_equal(emailTopicNameMap_new[order(names(emailTopicNameMap_new))],emailTopicNameMap[order(names(emailTopicNameMap))])
})

test_that("test result is the same as the one saved for returned spark data frame ", {
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactions.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_accountProduct.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactionsP.RData', homedir))
  # convert spark dataframe to data table
  interactions_new <- convertSDFToDF(interactions_new)
  interactionsP_new <- convertSDFToDF(interactionsP_new)
  accountProduct_new <- convertSDFToDF(accountProduct_new)
  # remove key setting in accountProduct in data.table
  setkey(accountProduct,NULL)
  # tests
  expect_setequal(names(interactions_new), names(interactions))
  expect_equal(interactions_new[order(interactionId, accountId, repId, messageId, physicalMessageUID, productInteractionTypeName, date),names(interactions), with=F],interactions[order(interactionId, accountId, repId, messageId, physicalMessageUID, productInteractionTypeName, date)])
  expect_setequal(names(accountProduct_new), names(accountProduct))
  expect_equal(accountProduct_new[order(accountId),names(accountProduct),with=F],accountProduct[order(accountId)])
  expect_setequal(names(interactionsP_new), names(interactionsP))
  expect_equal(interactionsP_new[order(interactionId, accountId, repId, messageId, physicalMessageUID, productInteractionTypeName, date),names(interactionsP), with=F],interactionsP[order(interactionId, accountId, repId, messageId, physicalMessageUID, productInteractionTypeName, date)])
})

# disconnect spark
closeSpark(sc)
