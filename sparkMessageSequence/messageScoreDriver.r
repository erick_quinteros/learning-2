##########################################################
#
#
# aktana- messageSequence scoring module
#
# description: driver pgm
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
library(data.table)
library(h2o)
library(uuid)
library(futile.logger)
library(openxlsx)
library(Learning)

#########################################################
## function: process score - score model and save scores
#########################################################
processScores <- function(isNightly, con, con_l, con_stage, con_cs, RUN_UID, BUILD_UID, config, runSettings, msgAlgoId, msgSetId, scoreDate, resetScoreTable)
{
  # get productUID which needed for loadMSOData
  productUID <- getConfigurationValueNew(config,"productUID")
  
  # read the data that needed for both manual and nighlty scoring
  startTimer <- Sys.time()
  dta <- loadMessageSequenceData(con, con_stage, con_cs, productUID, readDataList=c("interactions","accountProduct","products","emailTopicNames","messages","messageSetMessage","interactionsP","expiredMessages"))
  for(i in 1:length(dta))assign(names(dta)[i],dta[[i]])
  ##### clean up memory
  rm(dta)
  ##### 
  flog.info("Data Read Time = %s",Sys.time()-startTimer)
  
  pName <- products$productName
  flog.info("Data loaded")
  
  ## set default default value to the arguments needed to run scoreModel (used in nightly scoring with flagRescoring=TRUE)
  msmMinCnt <- NULL
  
  ## check whether rescoring or not
  flagRescoring  <- getConfigurationValueNew(config,"LE_MS_flagRescoring",convertFunc=as.logical, defaultValue=FALSE)
  flog.info("Runing scoring job with flagRescoring=%s",flagRescoring)
  
  ## compose target message list (TargetNames) for scoring
  if (isNightly) {
    # for nightly job, read from MessageSetMessage table, and get TargetNames only associated with messageSetId in MessageAlgorithm
    if(nrow(messageSetMessage)>0) {
      msgIds <- messageSetMessage[messageSetId %in% msgSetId]$messageId
      TargetNames <- unique(messages[productName==pName & !is.na(lastPhysicalMessageUID) & messageId %in% msgIds]$lastPhysicalMessageUID)
      
      if(flagRescoring) {
        # build a table for each message with minimum count of number of messages in corresponding messageSets to be scored.
        # e.g. if msg# 20 belongs to messageSet 1 (containing 6 msgs) and messageSet2 (containing 8 msgs), the tuple should be (20, 6) 
        msm <- messageSetMessage[messageSetId %in% msgSetId]  # messageSetMessage filtering with msgAlgoId and those messageSetIds
        msmCnt <- msm[, .N, by="messageSetId"]             # group by messageSetId; count of messages in each messageSetId
        msm <- merge(msm, msmCnt)                          # tuple(messageSetId, messageId, Count) 
        msmMinCnt <- msm[, .(cnt=min(N)), by="messageId"] # tuple(messageId, minimum count)
      }
    } else {
      flog.warn("messageSetMessage table in learning DB is empty")
    }
  } else {
    targetMesseges <-messages[messages$messageChannelId==1,]
    TargetNames <- unique(targetMesseges[productName==pName & !is.na(lastPhysicalMessageUID)]$lastPhysicalMessageUID)
  }
  
  print("TargetNames are: ")
  print(TargetNames)
  
  ## get whitelistDB
  whiteList <- whiteListNew(con, con_l, RUN_UID, BUILD_UID, config[["productUID"]], config[["LE_MS_WhiteListType"]], config[["LE_MS_WhiteList"]])$accountId
  
  ## score the model
  startTimer <- Sys.time()
  flog.info("Start scoring, score at date=%s",strftime(scoreDate))
  result <- scoreModel(con_l, isNightly, flagRescoring, runSettings[["runDir"]], runSettings[["modelsaveDir"]], runSettings[["modelsaveSpreadsheetName"]], BUILD_UID, config, scoreDate, TargetNames, whiteList, products, accountProduct, interactions, interactionsP, messages, expiredMessages, emailTopicNames, msmMinCnt)
  flog.info("Score Time = %s",Sys.time()-startTimer)
  flog.info("Result[[1]]= %s",result[[1]])
  
  ## save the result
  if(result[[1]]==0)  # 0 means there are scores returned
  {
    scores <- result[[2]]
    messageRescoringTimesNew <- result[[3]]
    # save scores
    saveScoreModelPlot(scores, RUN_UID, runSettings[["plotDir"]])
    saveScoreModelExcel(scores, pName, runSettings[["WORKBOOK"]], runSettings[["spreadsheetName"]])
    saveScoreModelDB(isNightly, con, con_l, scores, whiteList, RUN_UID, BUILD_UID, config[["configUID"]], msgAlgoId, messageRescoringTimesNew, resetScoreTable)
    resetScoreTable <<- FALSE # for second save scores not truncate table
  }
  
  flog.info("Back from scoring and score save...")
}


###########################
## Main program
###########################

# parameter defaults
batch <- TRUE
DRIVERMODULE <- "messageScoreDriver.r"

options(rgl.useNULL=TRUE)
Sys.setenv("R_ZIPCMD"="/usr/bin/zip")

print("start h2o early")
tryCatch(suppressWarnings(h2o.init(nthreads=-1, max_mem_size="21g")), 
         error = function(e) {
           flog.error('Error in h2o.init()',name='error')
           quit(save = "no", status = 65, runLast = FALSE) # user-defined error code 65 for failure of h2o initialization
         })

# set parameters 
args <- commandArgs(T)

if(length(args)==0){
    print("No arguments supplied.")
    if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
    print("Arguments supplied.")
    for(i in 1:length(args)){
       eval(parse(text=args[[i]]))
       print(args[[i]]);
    }
}
# config port param to be compatibale to be passed into dbConnect
if (!exists('port')) {
  port <- 0
} else {
  port <- as.numeric(port)
}

isNightly <- F
if(!exists("BUILD_UID")) isNightly <- T

# prepare scoreDate
if(!exists("scoreDate")) scoreDate <- Sys.Date()

# replace cleanup function to save failure in learning DB
cleanUpMSOScore <- function () 
{   
  tryCatch ({
    # update learningBuild table
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
    # update learningRun table for manual scoring job
    SQL <- sprintf("UPDATE LearningRun SET executionDateTime='%s', executionStatus='failure' WHERE learningRunUID='%s';",now,RUN_UID)
    dbGetQuery(con_l, SQL)
  },
  error = function(e){flog.error("Running Error Handler failed: %s", e)},
  finally = {
    # disconnet DB and exit
    dbDisconnect(con)
    dbDisconnect(con_l)
    dbDisconnect(con_stage)
    dbDisconnect(con_cs)
    q('no',status=1)}
  )
  # exit()
}

# load the read data and score and save code
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf("%s/messageSequence/whiteList.r",homedir)) 
source(sprintf("%s/messageSequence/loadMessageSequenceData.R",homedir))
source(sprintf("%s/messageSequence/scoreModel.R",homedir))
source(sprintf("%s/messageSequence/saveScoreModel.R",homedir))
source(sprintf("%s/messageSequence/messageSequenceHelper.R",homedir))

# establish db connections
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
con_stage <- getDBConnectionStage(dbuser, dbpassword, dbhost, dbname, port)
con_cs <- getDBConnectionCS(dbuser, dbpassword, dbhost, dbname, port)


if (isNightly) 
{ 
  # get messageAlgorithmIds from DSE table MessageAlgorithm and MessageSet
  modelsToScore <- data.table(dbGetQuery(con,"SELECT ms.messageSetId, ms.messageAlgorithmId, ma.externalId FROM MessageSet ms JOIN (SELECT messageAlgorithmId, externalId FROM MessageAlgorithm where isDeleted=0 and isActive=1 and messageAlgorithmType=1) ma ON ms.messageAlgorithmId=ma.messageAlgorithmId;")) # natural inner-join; could be empty if non-match
  # check whether modelsToScore is empty, if truethen stop
  if (nrow(modelsToScore)>0) {
    uniqueMessageAlgorithm <- unique(modelsToScore$messageAlgorithmId)
  } else {
    flog.error("no modelsToScore for the nightly job")
    stop("no modelsToScore for the nightly job")
  }
  
  # log which algorithm will be run
  print("The following messageAlgorithm(id) will be looped")
  print(uniqueMessageAlgorithm)
  
  # truncate score table for the first save score for nighlty scoring
  resetScoreTable <<- TRUE
  
  # now loop through MessageAlgorithm
  for(msgAlgoId in uniqueMessageAlgorithm)
  {
    CONFIG_UID <- unique(modelsToScore[messageAlgorithmId==msgAlgoId]$externalId)   # get config to score nighlty
    msgSetId <- unique(modelsToScore[messageAlgorithmId==msgAlgoId]$messageSetId) 

    config <- initializeConfigurationNew(homedir, CONFIG_UID, inNightlyFolder=TRUE)
    if (is.null(config)) {
        flog.info("The learning.properties for the following config %s doesn't exist!", CONFIG_UID)
        next # skip this msgAlgoId
    }
    
    # Check if the learning.properties mentions this model as an optimized model 
    isOptimized <- getConfigurationValueNew(config, "LE_MS_isOptimizedVersion", as.logical, FALSE)
    if(isOptimized)
    {
      # Update the config to get the optimal parameters from database
      config <- updateLearningConfigForOptimalParameters(config, con_l)
    }

    flog.info("Start loop next msgAlgoId = %s", msgAlgoId)
    
    # generate a unique runuid for each scoring job
    RUN_UID <- UUIDgenerate()
    # log start of scoring in DB
    BUILD_UID <- config[["buildUID"]]
    VERSION_UID <- config[["versionUID"]]
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
    SQL <- sprintf("INSERT INTO LearningRun(learningRunUID,learningBuildUID,learningVersionUID,learningConfigUID,isPublished,runType,executionStatus,executionDateTime) VALUES('%s','%s','%s','%s',1,'MSO','running','%s');", RUN_UID,BUILD_UID,VERSION_UID,CONFIG_UID,now)
    flog.info("INSERT INTO LearningRun for nightly scoring %s",CONFIG_UID)
    dbGetQuery(con_l, SQL)

    # initialize the client
    runSettings <- initializeClientNew(DRIVERMODULE, homedir, BUILD_UID, RUN_UID, errorFunc=cleanUpMSOScore, createExcel=TRUE, createPlotDir=TRUE)
    runSettings[["modelsaveSpreadsheetName"]] <- sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID,BUILD_UID)
    
    # score model and save scores
    processScores(isNightly, con, con_l, con_stage, con_cs, RUN_UID, BUILD_UID, config, runSettings, msgAlgoId, msgSetId, scoreDate, resetScoreTable)

    # update the learning database with the run    
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
    SQL <- sprintf("UPDATE LearningRun SET executionStatus='%s', executionDateTime='%s' WHERE learningRunUID='%s';", "success", now, RUN_UID)
    flog.info("Update status(success) in LearningRun.")
    dbGetQuery(con_l,SQL)

    config <- NULL
  } # end loop for msgAlgoId
}

if (!isNightly) 
{ # Not nightly job - manual run with BUILD_UID

  config <- initializeConfigurationNew(homedir, BUILD_UID, inNightlyFolder=FALSE)
  if (is.null(config)) {
    flog.info("The learning.properties for the following config %s doesn't exist!", BUILD_UID)
    break # break if statement
  }
  
  # Check if the learning.properties mentions this model as an optimized model 
  isOptimized <- getConfigurationValueNew(config, "LE_MS_isOptimizedVersion", as.logical, FALSE)
  if(isOptimized)
  {
    # Update the config to get the optimal parameters from database
    config <- updateLearningConfigForOptimalParameters(config, con_l)
  }
  
  # initialize the client
  runSettings <- initializeClientNew(DRIVERMODULE, homedir, BUILD_UID, RUN_UID, errorFunc=cleanUpMSOScore, createExcel=TRUE, createPlotDir=TRUE)
  runSettings[["modelsaveSpreadsheetName"]] <- sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID,BUILD_UID)

  # score model and save scores
  resetScoreTable <<- FALSE
  msgSetId <- 0 
  msgAlgoId <- 0
  processScores(isNightly, con, con_l, con_stage, con_cs, RUN_UID, BUILD_UID, config, runSettings, msgAlgoId, msgSetId, scoreDate, resetScoreTable)

  # update the learning database with the run
  now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
  SQL <- sprintf("UPDATE LearningRun SET executionDateTime='%s', executionStatus='success' WHERE learningRunUID='%s';",now,RUN_UID) # UI side already create entry so just update

  flog.info("UPDATE LearningRun SQL is: %s", SQL)
  dbGetQuery(con_l,SQL)

  config <- NULL
}

# Disconnet DB and release handles
dbDisconnect(con)
dbDisconnect(con_l)
dbDisconnect(con_stage)
dbDisconnect(con_cs)

# # shut down h2o
# h2o.shutdown(prompt=FALSE)

# final clean up
closeClient()