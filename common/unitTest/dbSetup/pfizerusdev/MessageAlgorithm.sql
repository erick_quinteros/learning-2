CREATE TABLE `MessageAlgorithm` (
  `messageAlgorithmId` int(11) NOT NULL AUTO_INCREMENT,
  `messageAlgorithmName` varchar(80) NOT NULL,
  `messageAlgorithmDescription` varchar(255) DEFAULT NULL,
  `productId` int(11) NOT NULL,
  `repActionChannelId` tinyint(4) DEFAULT NULL,
  `messageAlgorithmType` int(5) NOT NULL DEFAULT '0',
  `externalId` varchar(40) DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`messageAlgorithmId`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8