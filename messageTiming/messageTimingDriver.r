##########################################################
#
#
# aktana- messageTiming estimates estimates Aktana Learning Engines.
#
# description: estimate rep likelihood of engaging
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
library(RMySQL)
library(data.table)
library(openxlsx)
library(uuid)
library(h2o)
library(futile.logger)
library(jsonlite)
library(httr)
library(Learning)

###########################
## Main program
###########################

options(rgl.useNULL=TRUE)
Sys.setenv("R_ZIPCMD"="/usr/bin/zip")
print(sessionInfo())
#gcinfo(TRUE)  # print whenever GC triggeres

print("start h2o early")
tryCatch(h2o.init(nthreads=-1, max_mem_size="28g"), 
         error = function(e) {
           flog.error('Error in h2o.init()',name='error')
           quit(save = "no", status = 65, runLast = FALSE) # user-defined error code 65 for failure of h2o initialization
         })

options(rgl.useNULL=TRUE)
Sys.setenv("R_ZIPCMD"="/usr/bin/zip")

# set parameters 
args <- commandArgs(T)

if(length(args)==0){
    print("No arguments supplied.")
    if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
        print("Arguments supplied.")
        for(i in 1:length(args)){
          eval(parse(text=args[[i]]))
          print(args[[i]]);
    }
}

# config port param to be compatibale to be passed into dbConnect
if (!exists('port')) {
  port <- 0
} else {
  port <- as.numeric(port)
}

source(sprintf("%s/common/dbConnection/dbConnection.R", homedir))
source(sprintf("%s/messageTiming/messageTimingData.R",homedir))
source(sprintf("%s/messageTiming/messageTiming.R",homedir))
source(sprintf("%s/messageTiming/tteReport.R",homedir))
source(sprintf("%s/messageTiming/getTTEparams.R",homedir))

# establish db connection
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)

# parameter defaults
DRIVERMODULE <- "messageTimingDriver.r"
if(!exists("RUN_UID")) RUN_UID <- UUIDgenerate()

# replace cleanup function to save failure in learning DB
cleanUp <- function () 
{   tryCatch ({
    # update learningBuild table
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
    SQL <- sprintf("UPDATE LearningBuild SET executionStatus='failure', executionDateTime='%s' WHERE learningBuildUID='%s';",now,BUILD_UID)
    dbGetQuery(con_l, SQL)
    # update learningFile table
    logName <- sprintf("%s/log_%s.txt",runDir,paste("Build",timeStamp,sep="_"))
    printName <- sprintf("%s/print_%s.txt",runDir,paste("Build",timeStamp,sep="_"))
    blobIt <- function(name)
    {
      t0 <- readBin(name,what="raw",n=1e7)
      t1 <- paste0(t0,collapse="")
      return(t1)
    }
    outfile <- blobIt(logName)
    SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data) VALUES('%s',NULL,'%s','%s','%s','%s','%s');",UUIDgenerate(),BUILD_UID,"log","txt",object.size(outfile),outfile)
    dbGetQuery(con_l, SQL)
    outfile <- blobIt(printName)
    SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data) VALUES('%s',NULL,'%s','%s','%s','%s','%s');",UUIDgenerate(),BUILD_UID,"print","txt",object.size(outfile),outfile)
    dbGetQuery(con_l, SQL)
    CATALOGENTY <<- paste("Error RUN: ", CATALOGENTRY)},
    error = function(e){flog.error("Running Error Handler failed: %s", e)},
    finally = {
      # disconnet DB and exit
      dbDisconnect(con_l)
      dbDisconnect(con)
      q('no',status=1)}
    )
    # exit()
}

# now can proceed
# initialize the client
initializeClient()
flog.info("Run initialized at: %s",Sys.time())

config <- initializeConfigurationNew(homedir, BUILD_UID, inNightlyFolder=FALSE)
if (is.null(config)) {
    flog.warn("The learning.properties for the following config %s doesn't exist!", BUILD_UID)
    quit(save = "no", status = 1, runLast = FALSE) 
}

versionUID <- config[["versionUID"]]
configUID <- config[["configUID"]]

tteParams <- getTTEparams(config, "BUILD")

# read the data
startTimer <- Sys.time()
dta <- messageTimingData(con, tteParams)

interactions <- dta[[1]]
accountProduct <- dta[[2]]
products <- dta[[3]]
accounts <- dta[[4]]

names(accountProduct) <- gsub("-","_",names(accountProduct))  # remove -'s from names
##### clean up memory
rm(dta)
#####
print("memory after data read and cleanup")
print(sapply(ls(), function(x) as.numeric(object.size(eval(parse(text=x))))))

# estimate the models
startTimer <- Sys.time()
errorCode <- messageTiming(con, con_l, tteParams)

if (errorCode) {
  quit(save = "no", status = errorCode, runLast = FALSE)   # user-defined error code 72 for memory error
}
flog.info("Model Build Time = %s",Sys.time()-startTimer)

# shut down h2o
h2o.shutdown(prompt=FALSE)

# save stuff
flog.info("Save catalog and workbook")
write("_________________________________________________________",catalogFile, append = T)
write(CATALOGENTRY, catalogFile, append = T)
spreadsheetName <- sprintf("%s/%s_%s.xlsx", runDir, DRIVERMODULE, runStamp)

statusCode <- tryCatch(saveWorkbook(WORKBOOK, file = spreadsheetName, overwrite = T), 
    error = function(e) {
    flog.info("Original error message: %s", e)
    flog.error('Error in saveWorkbook',name='error')
    return (NA)
})

if (is.na(statusCode)) {
    quit(save = "no", status = 72, runLast = FALSE)   # user-defined error code 72 for memory error
}

# call deploy API on successful build
# initialize DEPLOY_ON_SUCCESS
if (!exists("deployOnSuccess")) {
  flog.info("deployOnSuccess not passed from API, set to FALSE")
  deployOnSuccess <- FALSE
} else {
  flog.info("deployOnSuccess passed from API, =%s", deployOnSuccess)
  deployOnSuccess <- as.logical(deployOnSuccess)
  if (is.null(deployOnSuccess)) {
    deployOnSuccess <- FALSE # set to false to non logical convertible value
    flog.info("deployOnSuccess set to False as not regonized as logical false")
  } else {
    flog.info("deplyOnSuccess set to %s as passed from API", deployOnSuccess)
  }
}

if (deployOnSuccess) {
  # source API call related scripts
  source(sprintf("%s/common/APICall/global.R",homedir))
  # initialize API call params
  init(TRUE, paste(homedir,"/common/APICall",sep=""))
  # Authenticate and get a token
  Authenticate(glblLearningAPIurl)
  # call deploy API
  req <- putAPIJson(glblLearningAPIurl, paste("LearningConfig/",configUID,"/versions/",versionUID,"/builds/",BUILD_UID,"/deploy",sep = ""))
}

# save model build results in the learning DB tables
logName <- sprintf("%s/log_%s.txt",runDir,paste("Build",timeStamp,sep="_"))
printName <- sprintf("%s/print_%s.txt",runDir,paste("Build", timeStamp,sep="_"))

blobIt <- function(name)
{
    t0 <- readBin(name,what="raw",n=1e7)
    t1 <- paste0(t0,collapse="")
    return(t1)
}

# update the learning DB File table

outfile <- blobIt(logName)
SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data) VALUES('%s',NULL,'%s','%s','%s','%s','%s');",UUIDgenerate(),BUILD_UID,"log","txt",object.size(outfile),outfile)
dbGetQuery(con_l, SQL)

outfile <- blobIt(printName)
SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data) VALUES('%s',NULL,'%s','%s','%s','%s','%s');",UUIDgenerate(),BUILD_UID,"print","txt",object.size(outfile),outfile)
dbGetQuery(con_l, SQL)

pNameUID <- tteParams[["pNameUID"]]
pName <- products[externalId==pNameUID]$productName

sheetname.A <- sprintf("%s_A", pName)
importance <- read.xlsx(spreadsheetName, sheet=sheetname.A)
tempName1 <- tempfile(fileext=".csv")
write.csv(importance,file=tempName1)
outfile <- blobIt(tempName1)
SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data) VALUES('%s',NULL,'%s','%s','%s','%s','%s');",UUIDgenerate(),BUILD_UID,"importance","csv",object.size(outfile),outfile)
dbGetQuery(con_l, SQL)

sheetname.A.ref <- sprintf("%s_A_reference", pName)
accuracy <- read.xlsx(spreadsheetName,sheet=sheetname.A.ref)
write.csv(accuracy,file=tempName1)
outfile <- blobIt(tempName1)
SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data) VALUES('%s',NULL,'%s','%s','%s','%s','%s');",UUIDgenerate(),BUILD_UID,"accuracy","csv",object.size(outfile),outfile)
dbGetQuery(con_l, SQL)

#### need to update the executionDateTime ####
now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
SQL <- sprintf("UPDATE LearningBuild SET executionStatus='%s', executionDateTime='%s' WHERE learningBuildUID='%s';","success",now,BUILD_UID)
dbGetQuery(con_l, SQL)

dbDisconnect(con_l)
dbDisconnect(con)
