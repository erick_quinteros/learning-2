"""
    This module builds dynamic model/design matrix/predictors
    based on historical data

    :copyright: AKTANA (c) 2018.
"""

import sys
import pandas as pd
from collections import deque
from itertools import accumulate
from collections import Counter


VAL_NO_DATA = 999   # default value representing no data
LOWER_BOUND = 30    # days to look back when counting SENDs
UPPER_BOUND = 60    # days to look forward when counting SENDs


def avg_prob_send(design):
    """
    Calculate average probability of Sends in all data
    :param design: interaction data
    :return: avg
    """
    send_cnt = sum(design['type'] == 'SEND')
    target_cnt = sum(design['type'] == 'TARGET')

    if send_cnt == 0:
        avg = 0.0
    else:
        avg = target_cnt / send_cnt

    return avg


def make_build_init(ev_type):          # utilizing function currying
    """
    maker for calling build_init_features
    :param ev_type: SEND/VISIT
    :return: build_init_features (calling this function)
    """
    def build_init_features(matrix):
        """
        1st pass to build preS, preV, preW, preC features for SEND and VISIT, given a dataframe.
        :param matrix: matrix of dictionary of lists
        """
        # initialize variables
    
        dates, types = matrix['date'], matrix['type']

        to_modify = None
        to_modify2 = None

        if ev_type == "SEND":
            to_modify = matrix['preSEND']
            to_modify2 = matrix['pre2S']
        elif ev_type == "VISIT":
            to_modify = matrix['preVISIT']
            to_modify2 = matrix['pre2V']
        else:
            tmp = 'pre' + ev_type
            to_modify = matrix[tmp]
            to_modify2 = matrix[tmp][:]

        pre_date = ''       # initialize variable holding previous Send/Visit date
        pre_date2 = ''

        for i in range(len(types)):

            to_modify[i] = (dates[i] - pre_date).days if pre_date else VAL_NO_DATA
            to_modify2[i] = (dates[i] - pre_date2).days if pre_date2 else VAL_NO_DATA

            if types[i] == ev_type:
                pre_date2 = pre_date  # state machine - shift
                pre_date = dates[i]

            # Process SEND and VISIT event/interaction
            # We will filter out TARGET rows later

    return build_init_features


def update_target_for_send(matrix):
    """
    2nd pass to update TARGET for SENDs
    :param matrix: matrix of dictionary of lists
    """
    # initialize variables
    dates, types, targets = matrix['date'], matrix['type'], matrix['TARGET']

    # initialize a stack
    stack = deque()

    for i in range(len(types)):

        if types[i] == 'SEND':
            stack.append((dates[i], i))

        elif types[i] == 'TARGET':
            # the one on top of stack is too old (e.g. 60 days ago), so empty the stack.
            # the default is 0, so no need to assign 0.
            if stack and (dates[i] - stack[-1][0]).days > UPPER_BOUND:
                stack = deque()
            elif stack:
                # stack is not empty, then pop the top one, and assign 1 to it.
                targets[stack.pop()[1]] = 1


def update_target_for_visit(matrix):
    """
    3rd pass to update TARGET for VISITs
    :param matrix: matrix of dictionary of lists
    """
    # initialize variables
    dates, types, targets = matrix['date'], matrix['type'], matrix['TARGET']

    # lower and upper bounds of moving windows to calculate probabilities of VISITs
    # When finding a Visit, we look for number of Sends in previous LOWER_BOUND days and next UPPER_BOUND days,
    # calculate the average probability of Sends and assign the value to TARGET for this Visit.
    # The assumption is a Visit would have more impact on later Open/Click rate. A doctor may not
    # goes back 30 days to open an old email after a rep visit, but more likely to open/click email
    # even 60 days later after a rep visit.

    lower, upper = 0, 0    # initialize to index of start
    num_rows = len(types)

    for i in range(num_rows):

        if types[i] == 'VISIT':

            while (dates[i] - dates[lower]).days > LOWER_BOUND:
                # increment lower bound if it's too old
                lower += 1

            while (upper < num_rows - 1) and (dates[upper] - dates[i]).days < UPPER_BOUND:
                # increment upper bound if it's not too far away
                upper += 1

            # count number of Sends between lower and upper bound of window
            send_cnt = sum(1 if types[j] == 'SEND' else 0 for j in range(lower, upper))

            if send_cnt:

                # count Target only for SEND event
                target_cnt = sum(targets[j] if types[j] == 'SEND' else 0 for j in range(lower, upper))

                # assign Target probability for this Visit
                targets[i] = target_cnt/send_cnt

            else:
                # no Send in the range; mark it with -1 to remove the Visit data later
                # (not using average probability of Sends to assign Target value for this Visit)
                targets[i] = -1


def count_svt(matrix):
    """
    4th pass to count the number of sends, visits, targets
    :param matrix: matrix of dictionary of lists
    """
    types = matrix['type']

    matrix['numS'] += accumulate(1 if t == 'SEND' else 0 for t in types)
    matrix['numV'] += accumulate(1 if t == 'VISIT' else 0 for t in types)
    matrix['numT'] += accumulate(1 if t == 'TARGET' else 0 for t in types)


def build_dynamic_model(design):
    """
    Input data looks like:
    accountId    type       date
       1002      SEND     2018-06-01
       1002      SEND     2018-07-03
       1002      VISIT    2018-07-13
       1002      TARGET   2018-07-14

    Output model looks like:
    accountId  event  preS  preV  preW  preC  TARGET
       1002     S     -1    -1     -1    -1    0.00
       1002     S     32    -1     20     9    0.0
       1002     V      3    -1     11     8    0.75
       1002     S      9     6      7    10    1.00


    :param design: dataframe of interaction data, sorted by accountId and date
    :return: feature matrix
    """

    nrows = design.shape[0]    # number of rows in dataframe

    # build a dictionary matrix {column -> list()}
    matrix = {}

    matrix['accountId'] = design['accountId'].tolist()
    matrix['date'] = design['date'].tolist()
    matrix['type'] = design['type'].tolist()
    matrix['event'] = design['type'].replace({'SEND':'S', 'VISIT':'V'}).tolist()

    typelist_all = Counter(matrix['type'])
    # typelist = set()
    # for k, v in typelist_all.items():
    #     if v >= 30:
    #         typelist.add(k)
    typelist = set(matrix['type'])

    for t in typelist:
        vname = "pre" + t
        matrix[vname] = [VAL_NO_DATA] * nrows
    matrix['pre2V'] = [VAL_NO_DATA] * nrows
    matrix['pre2S'] = [VAL_NO_DATA] * nrows
    matrix['numS'] = []
    matrix['numV'] = []
    matrix['numT'] = []
    matrix['TARGET'] = [0.0 for _ in range(nrows)]

    # get the average probability of sends
    global avg_s
    avg_s = avg_prob_send(design)

    # build the ranges of <start, end> index in dataframe for each account
    ranges = []
    start = 0

    for i in range(1, design.shape[0]):
        if matrix['accountId'][i] != matrix['accountId'][i-1]:
            ranges.append((start, i))
            start = i
    ranges.append((start, nrows))

    # process each account to build features
    for start, end in ranges:
        matrix_range = {}
        for field in matrix:
            matrix_range[field] = matrix[field][start:end]


        for t in typelist:
            make_build_init(t)(matrix_range)

        update_target_for_send(matrix_range)
        update_target_for_visit(matrix_range)
        count_svt(matrix_range)

        for field in matrix_range:
            matrix[field][start:end] = matrix_range[field]


    # collect data
    
    res = pd.DataFrame.from_dict(matrix)

    res['date'] = pd.to_datetime(res['date'], format='%Y-%m-%d')
    # filter out rows
    res = res.loc[res['type'].isin(["SEND", "VISIT"])]

    # add Day of Week feature
    res['dow'] = res['date'].dt.strftime('%a')

    # put 'dow' 'date' 'TARGET' at the end
    res.pop('type')
    for t in ['dow', 'date', "TARGET"]:
        tmp = res.pop(t)
        res[t] = tmp

    return res

