##########################################################
#
#
# aktana- messageTiming Scoring module.
#
# description: driver pgm
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
library(RMySQL)
library(data.table)
library(openxlsx)
library(h2o)
library(properties)
library(uuid)
library(futile.logger)
library(Learning)
library(sparkLearning)
library(sparklyr)
library(dplyr)
library(arrow)

#################################################
## function: cleanUp (errorhandler function)
#################################################
cleanUp <- function () 
{   
  tryCatch ({
    # update learningBuild table
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
    # update learningRun table for manual scoring job
    if(!isNightly) {
      SQL <- sprintf("UPDATE LearningRun SET executionDateTime='%s', executionStatus='failure' WHERE learningRunUID='%s';",now,RUN_UID) 
      dbGetQuery(con_l, SQL)
    }
  },
  error = function(e){flog.error("Running Error Handler failed: %s", e)},
  finally = {
    # disconnet DB and exit
    dbDisconnect(con_l)
    dbDisconnect(con)
    q('no',status=1)}
  )
  # exit()
}


#########################################################
## function: process score - score model and save scores
#########################################################
processMTScores <- function(con, con_l, RUN_UID, BUILD_UID, CONFIG_UID, productUID, isNightly, runSettings, tteParams)
{
    # read the data
    startTimer <- Sys.time()
    dta <- messageTimingData(con, tteParams)
    interactions <<- dta[[1]]
    accountProduct <<- dta[[2]]
    products <<- dta[[3]]
    accounts <<- dta[[4]]

    ##### clean up memory
    rm(dta)
    ##### 
    flog.info("Data Read Time = %s",Sys.time()-startTimer)

    #names(accountProduct) <- gsub("-","_",names(accountProduct))  # remove -'s from names
    flog.info("Data loaded")

    # get whitelistDB
    whiteListDB <- whiteList(con, con_l, RUN_UID, BUILD_UID, CONFIG_UID, dbname, homedir) 
    
    startTimer <- Sys.time()

    ## score the model
    result <- messageTimingScoreModel(con, con_l, runSettings, tteParams, whiteListDB)

    flog.info("Score Time = %s",Sys.time()-startTimer)
    flog.info("Result[[1]]= %s",result[[1]])

    ## save the result
    if(result[[1]]==0)  # 0 means there are scores returned
    {
        t <- result[[2]] # scores   

        saveTimingScoreResult(con, con_l, t, BUILD_UID, RUN_UID, productUID, isNightly, runSettings, tteParams)

        rm(t)  # force release memory from variable scores which is no longer needed.
        rm(result)
        gc(); gc(); gc()        
    } 
        
    flog.info("Back from MT scoring and score save...")
}

###########################
## Main program
###########################

# parameter defaults
MODEL <- "None"
batch <- TRUE
DRIVERMODULE <- "messageTimingScoreDriver.r"

options(rgl.useNULL=TRUE)
Sys.setenv("R_ZIPCMD"="/usr/bin/zip")

print("start h2o early")
tryCatch(h2o.init(nthreads=-1, max_mem_size="21g"), 
         error = function(e) {
           flog.error('Error in h2o.init()',name='error')
           quit(save = "no", status = 65, runLast = FALSE) # user-defined error code 65 for failure of h2o initialization
         })

# set parameters 
args <- commandArgs(T)

if(length(args)==0){
    print("No arguments supplied.")
    if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
    print("Arguments supplied.")
    for(i in 1:length(args)){
       eval(parse(text=args[[i]]))
       print(args[[i]]);
    }
}

# config port param to be compatibale to be passed into dbConnect
if (!exists('port')) {
  port <- 0
} else {
  port <- as.numeric(port)
}

# load the read data and score and save code
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf("%s/messageTiming/messageTimingData.R",homedir))
source(sprintf("%s/messageTiming/messageTimingScoreModel.R",homedir))
source(sprintf("%s/messageTiming/tteMethods.R",homedir))
source(sprintf("%s/messageTiming/tteReport.R",homedir))
source(sprintf("%s/messageTiming/saveTimingScoreResult.R",homedir))
source(sprintf("%s/messageTiming/saveTimingScoreReport.R",homedir))
source(sprintf("%s/messageTiming/getTTEparams.R",homedir))
#source(sprintf("%s/messageTiming/buildStaticFeatures.R",homedir))

if (!exists("RUN_UID")) {
  RUN_UID <- ""
} # initialize RUN_UID so that next source command won't get error of undefined RUN_UID
source(sprintf("%s/messageSequence/whiteList.r",homedir))   

if (!exists("runmodel")) {
  runmodel <- "NORMAL"
}

# establish db connections
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)

## get spark connection
## if spark conext sc exists, skip connecting. 
## Note that here sc is special object name for spark context
if (!exists('sc')) {  
  sc <- initializeSpark(homedir, master="yarn-client", version="2.3.1")
}

isNightly <<- F
if(!exists("BUILD_UID"))isNightly <<- T

#### pull out the right configs and models
if(isNightly) # read learning.properties from nightly/configUID directory
{
  # read config data from MessageAlgorithm
  modelsToScore <- data.table(dbGetQuery(con,"Select tteAlgorithmId, externalId, tteAlgorithmName from TimeToEngageAlgorithm where isDeleted=0 and isActive=1"))

  # check whether modelsToScore is empty, if true then stop
  if (nrow(modelsToScore)>0) {
      uniqueTTEAlgorithm <- unique(modelsToScore$tteAlgorithmId)
          
      # log which algorithm will be run
      print("The following tteAlgorithm(id) will be looped")
      print(uniqueTTEAlgorithm)
  } else {
      flog.error("no modelsToScore for the nightly job")
      stop("no modelsToScore for the nightly job")
  }

  for(tteAlgoId in uniqueTTEAlgorithm)
  {
      CONFIG_UID <- unique(modelsToScore[tteAlgorithmId==tteAlgoId]$externalId)   # get config to score nighlty

      config <- initializeConfigurationNew(homedir, CONFIG_UID, inNightlyFolder=TRUE)
      if (is.null(config)) {
          flog.warn("The learning.properties for the following config %s doesn't exist!", CONFIG_UID)
          next # skip this tteAlgoId
      }

      tteAlgorithmName <- modelsToScore[tteAlgorithmId==tteAlgoId]$tteAlgorithmName

      tteParams <- getTTEparams(config, "SCORE")
      tteParams[["tteAlgorithmName"]] <- tteAlgorithmName  # add tteAlgorithmName to tteParams in the end

      if (runmodel == "REPORT" && !tteParams[["isReportOnly"]]) {
          next  # skip if reporting Rundeck job && isReportOnly=F (DSE run for nightly scoring)
      }

      flog.info("Start loop next tteAlgoId = %s", tteAlgoId)

      # generate a unique runuid for each scoring job
      RUN_UID <- UUIDgenerate()
      BUILD_UID <- config[["buildUID"]]
      VERSION_UID <- config[["versionUID"]]
      productUID  <- config[["productUID"]]

      now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
      SQL <- sprintf("INSERT INTO LearningRun(learningRunUID,learningBuildUID,learningVersionUID,learningConfigUID,isPublished,runType,executionStatus,executionDateTime) VALUES('%s','%s','%s','%s',1,'TTE','running','%s');", RUN_UID,BUILD_UID,VERSION_UID,CONFIG_UID,now)
      flog.info("INSERT INTO LearningRun for TTE nightly scoring %s",CONFIG_UID)
      dbGetQuery(con_l, SQL)

      # initialize the client
      runSettings <- initializeClientNew(DRIVERMODULE, homedir, BUILD_UID, RUN_UID, errorFunc=cleanUp, createExcel=TRUE, createPlotDir=TRUE)
      runSettings[["modelsaveSpreadsheetName"]] <- sprintf('%s/builds/%s/messageTimingScoreDriver.r_%s.xlsx',homedir,BUILD_UID,BUILD_UID)

      # score model and save scores
      processMTScores(con, con_l, RUN_UID, BUILD_UID, CONFIG_UID, productUID, isNightly, runSettings, tteParams)
      
      SQL <- sprintf("UPDATE LearningRun SET executionDateTime='%s', executionStatus='success' WHERE learningRunUID='%s';",now,RUN_UID)

      flog.info("INSERT INTO LearningRun SQL is: %s", SQL)

      dbGetQuery(con_l, SQL)

  }
} 

if(!isNightly) # manual run
{
  config <- initializeConfigurationNew(homedir, BUILD_UID, inNightlyFolder=FALSE)
  if (is.null(config)) {
    flog.warn("The learning.properties for the following config %s doesn't exist!", BUILD_UID)
    break # break if statement
  }

  CONFIG_UID <- config[["configUID"]]

  # initialize the client
  runSettings <- initializeClientNew(DRIVERMODULE, homedir, BUILD_UID, RUN_UID, errorFunc=cleanUp, createExcel=TRUE, createPlotDir=TRUE)
  runSettings[["modelsaveSpreadsheetName"]] <- sprintf('%s/builds/%s/messageTimingScoreDriver.r_%s.xlsx',homedir,BUILD_UID,BUILD_UID)

  tteParams <- getTTEparams(config, "SCORE")  

  # score model and save scores
  processMTScores(con, con_l, RUN_UID, BUILD_UID, CONFIG_UID, "AKT_ALL_PRODUCTS", isNightly, runSettings, tteParams)

  now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
  SQL <- sprintf("UPDATE LearningRun SET executionDateTime='%s', executionStatus='success' WHERE learningRunUID='%s';",now,RUN_UID) # UI/API side already create entry so just update
  dbGetQuery(con_l, SQL)

}

#close open files
sink()

# Disconnet DB and release handles
dbDisconnect(con)
dbDisconnect(con_l)

closeSpark(sc)
closeClient()
# shut down h2o
#h2o.shutdown(prompt=FALSE)
