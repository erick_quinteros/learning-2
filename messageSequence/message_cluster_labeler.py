"""
    This module labeled the emailTopic given by messageClustering

    :copyright: AKTANA (c) 2018.
"""

import pandas as pd
from gensim.parsing.preprocessing import remove_stopwords
from gensim.parsing.preprocessing import strip_multiple_whitespaces
from nltk import FreqDist
from collections import Counter
import traceback


def generate_label_v2(message_text, product_name):
    """
    This is label generator for provided message text and product name.
    :param message_text:
    :param product_name:
    :return:
    """
    # Remove stop words from the text
    message_text = remove_stopwords(message_text)

    # Make the text in lower case to make it easy to replace and process
    message_text = message_text.lower().replace(product_name.lower(), " ")

    # Remove special characters
    message_text = message_text.translate({ord(c): " " for c in "!@#$%^&*[]{};:,./<>?\|`~=_+"})
    message_text = message_text.replace('"', " ")

    # Replace specific context words
    message_text = message_text.replace("rte",  " ")
    message_text = message_text.replace("®",  " ")
    message_text = message_text.replace("customtext",  " ")

    message_text = message_text.replace("follow up",  "follow-up")

    # Remove the unnecessary white-spaces in the text
    message_text = strip_multiple_whitespaces(message_text)

    # Calculate the frequency distribution of words
    freq_dist = FreqDist(message_text.split(" "))
    word_list = [item[0] for item in freq_dist.most_common(3)]

    # Generate a label
    label = " ".join(word_list)

    # Append the product name to the label
    label = product_name + " " + label

    # Return the label in 'Title Case' for better readability
    return label.title()


def rename_duplicates(message_label_list):
    """
    This function renames the duplicate by adding occurance count at the end
    input: ["name", "state", "name", "city", "name", "zip", "zip"]
    output: ['name1', 'state', 'name2', 'city', 'name3', 'zip1', 'zip2']
    """
    counts = {k: v for k, v in Counter(message_label_list).items() if v > 1}
    unique_message_list = message_label_list[:]

    for i in reversed(range(len(message_label_list))):
        item = message_label_list[i]
        if item in counts and counts[item]:
            unique_message_list[i] += ' {}'.format(counts[item])
            counts[item] -= 1
    return unique_message_list


def generate_message_cluster_labels(message_topic_cluster_df, product_name_df):
    """
    function to generate message cluster labels given message_topic_cluster_df from messageClustering
    :param message_topic_cluster_df: message_topic_cluster_df with columns ("documentDescription", "emailSubject", "productUID", "emailTopicId")
    :param product_name_df: product_name_df with columns ("productName", "externalId")
    :return: message_topic_cluster_df with added column "emailTopicName" if email topic generated else returns same data frame.
    """
    try:
        # Concat the message subject strings to get the text
        message_topic_cluster_df['text'] = message_topic_cluster_df.documentDescription.str.cat(message_topic_cluster_df.emailSubject, sep=" ")

        # For non-latin languages some values in text column are not used as string hence explicit conversion
        message_topic_cluster_df['text'] = message_topic_cluster_df['text'].apply(str)

        message_topic_cluster_df = message_topic_cluster_df.groupby(['productUID', 'emailTopicId'])['text'].apply(lambda x: "%s" % '. '.join(x)).reset_index()

        # Apply generate label function to the text for each message
        message_topic_cluster_df['emailTopicName'] = message_topic_cluster_df.apply(
            lambda x: generate_label_v2(x['text'], product_name_df[product_name_df['externalId'] == x['productUID']][
                'productName'].values[0] if any(product_name_df['externalId'] == x['productUID']) else 'Other'), axis=1)

        # Remove the text column
        message_topic_cluster_df.drop(['text'], axis=1, inplace=True)

        # rename duplicates in the labeling results
        message_topic_cluster_df['emailTopicName'] = rename_duplicates(message_topic_cluster_df['emailTopicName'])

    except:
        """
        NOTE: In case there is exception while generating email cluster labels. Attempt to generate default labels like 
        'Message Topic 1', 'Message Topic 2' etc.
        """
        try:
            traceback.print_exc()
            print('Encountered error in generating message topic labels. Generating default message topic labels..')
            message_topic_cluster_df.drop(columns=['messageUID', 'documentDescription', 'emailSubject', 'text'], inplace=True, errors='ignore')
            message_topic_cluster_df.drop_duplicates(subset=['productUID', 'emailTopicId'], keep='last', inplace=True)
            message_topic_cluster_df = message_topic_cluster_df.reset_index(drop=True)
            message_topic_cluster_df['emailTopicName'] = 'Message Topic ' + message_topic_cluster_df['emailTopicId'].astype(str)

            print('Default labels generated.')
        except:
            """
            NOTE: In case the default label generation fails. Print the stack trace and print the error message. 
            Cluster labeler will not exit the system in that case MSO may able to continue without the labels. If handled
            at calling R module.
            """
            traceback.print_exc()
            print('Error - Unable to generate default message topics labels.')

    return message_topic_cluster_df
