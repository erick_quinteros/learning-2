context('test the whiteList script in the messageSequence module')
print(Sys.time())

# source scripts needed to run tests
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))

# writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
requiredMockDataList <- list(pfizerusdev=c('RepAccountAssignment','Account','Product','AccountProduct','Rep'),pfizerusdev_learning=c('LearningObjectList'))
resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)

# set up unit test build folder and related configs
# get buildUID
BUILD_UID <<- readModuleConfig(homedir, 'messageSequence','buildUID')
# add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
setupMockBuildDir(homedir, 'messageSequence', BUILD_UID, files_to_copy='learning.properties')

# get required arguemtns
library(uuid)
library(RMySQL)
RUN_UID <- UUIDgenerate()
library(properties)
propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,BUILD_UID)
CONFIG_UID <- read.properties(propertiesFilePath)[["configUID"]]
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)

# source funcs
source(sprintf("%s/messageSequence/whiteList.r",homedir))

# run func
whiteListDB_new <- whiteList(con, con_l, RUN_UID, BUILD_UID, CONFIG_UID, dbname, homedir)

# disconnect DB
dbDisconnect(con)
dbDisconnect(con_l)

# test case
test_that("check whitelistDB has the right dimension", {
  expect_equal(dim(whiteListDB_new), c(309,6))
  # compare with saved result
  load(sprintf('%s/messageSequence/tests/data/from_whiteList_whiteListDB.RData', homedir))
  expect_equal(whiteListDB[order(whiteListDB$accountId),-c("learningRunUID")],whiteListDB_new[order(whiteListDB_new$accountId),-c("learningRunUID")])
})

test_that("correct entry write to learning DB", {
  drv <- dbDriver("MySQL")
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  SQL <- sprintf("SELECT * from LearningObjectList WHERE learningBuildUID='%s' and learningRunUID='%s';",BUILD_UID, RUN_UID)
  learningObjectList <- dbGetQuery(con_l, SQL)
  dbDisconnect(con_l)
  expect_equal(dim(learningObjectList), c(0,7)) # empty list for LE_MS_WhiteListType = "ALL"
  # expect_equal(learningObjectList$objectUID, whiteListDB$objectUID)

})

