context('test the saveScoreModelDB func in the messageSequence module')
print(Sys.time())

# loading library, common source script
library(uuid)
library(Learning)
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf("%s/messageSequence/saveScoreModel.R",homedir))

###################### func for resetting mock data for run saveScoreModel #######################
runSaveModelScoreMockDataReset <- function() {
  # writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  requiredMockDataList <- list(pfizerusdev=c('AccountMessageSequence','Account','Message'),pfizerusdev_learning=c('AccountMessageSequence','MessageRescoringTimes'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)
}

##################### func for prepare arguments necessary for run saveScoreModel func and run ###################
runSaveModelScore <- function(isNightly, resetScoreTable) {
  # db connection (manual need con_l, nightly need con)
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  # load data from scoreModel() result needed for running saveScoreModel, and other necessary arguemtns
  load(sprintf('%s/messageSequence/tests/data/from_whiteList_whiteListDB.RData', homedir)) # get whiteListDB
  whiteList <- whiteListDB$accountId
  BUILD_UID <- readModuleConfig(homedir, 'messageSequence','buildUID')  # manula use only
  CONFIG_UID <- initializeConfigurationNew(homedir, BUILD_UID)[["configUID"]] # nightly use only
  RUN_UID <- UUIDgenerate()
  if (isNightly) {
    load(sprintf('%s/messageSequence/tests/data/from_scoreModel_nightly_scores.RData', homedir))  # load scores
    load(sprintf('%s/messageSequence/tests/data/from_scoreModel_nightly_messageRescoringTimesNew.RData', homedir))  # load messageRescoringTimesNew
    msgAlgoId <- 23
  } else {
    load(sprintf('%s/messageSequence/tests/data/from_scoreModel_manual_scores.RData', homedir))  # load scores
    load(sprintf('%s/messageSequence/tests/data/from_scoreModel_manual_messageRescoringTimesNew.RData', homedir))  # load messageRescoringTimesNew
    msgAlgoId <- 0
  }
  # run saveScoreModel
  saveScoreModelDB(isNightly, con, con_l, scores, whiteList, RUN_UID, BUILD_UID, CONFIG_UID, msgAlgoId, messageRescoringTimesNew, resetScoreTable)
  # disconnect DB
  dbDisconnect(con)
  dbDisconnect(con_l)
}

################# main test procedures and cases #############################
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)

test_that('test for save for manual scoring', {
  # run saveModelScore()
  isNightly <- FALSE
  resetScoreTable <- FALSE
  runSaveModelScoreMockDataReset()
  runSaveModelScore(isNightly,resetScoreTable)
  
  # run tests on DB result
  accountMessageSequence <- dbGetQuery(con_l, 'SELECT * from AccountMessageSequence;')
  # test cases
  expect_equal(dim(accountMessageSequence), c(88,7))
})

test_that('test for run save for manual scoring again without cleaning DB (would add the same scores again as just append results)', {
  isNightly <- FALSE
  resetScoreTable <- FALSE
  runSaveModelScore(isNightly, resetScoreTable)
  
  # run tests on DB result
  accountMessageSequence <- dbGetQuery(con_l, 'SELECT * from AccountMessageSequence;')
  expect_equal(dim(accountMessageSequence), c(176,7))
})

test_that('test for save for nightly scoring', {
  # run saveModelScore()
  isNightly <- TRUE
  resetScoreTable <- TRUE
  runSaveModelScoreMockDataReset()
  runSaveModelScore(isNightly, resetScoreTable)
  
  # run tests on DB result
  accountMessageSequence <- dbGetQuery(con, 'SELECT * from AccountMessageSequence;')
  # load saved nightly scoring for comparison
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_nightly_scores.RData', homedir))
  savedScores <- setkey(scores,NULL)
  savedScores$probability <- savedScores$prob*savedScores$AUC
  savedScores$predictDB <- 1
  # test cases
  expect_equal(dim(accountMessageSequence), c(184,9))
  expect_equal(unname(accountMessageSequence[,c("accountId","probability","predict","messageId")]), unname(data.frame(savedScores[,c("accountId","probability","predictDB","msgId")])))
})

test_that('test for run save for nightly scoring again with resetScoreTable=TRUE without cleaning DB and add some test scores (would truncate table and replace the old scores in DB)', {
  isNightly <- TRUE
  resetScoreTable <- TRUE
  dbClearResult(dbSendQuery(con, "INSERT INTO AccountMessageSequence VALUES (1001,100,100,'test','test',0.9,1,'2012-12-01','2012-12-01');")) # add dummy data to test table will be truncate
  runSaveModelScore(isNightly, resetScoreTable)
  
  # run tests on DB result
  accountMessageSequence <- dbGetQuery(con, 'SELECT * from AccountMessageSequence;')
  expect_equal(dim(accountMessageSequence), c(184,9))
  messageRescoringTimes <- dbGetQuery(con_l, 'SELECT * from MessageRescoringTimes;')
  expect_equal(dim(messageRescoringTimes), c(106,7))
})

test_that('test for run save for nightly scoring again with resetScoreTable=FALSE without cleaning DB and add some test scores (would not truncate table and append new score in DB)', {
  isNightly <- TRUE
  resetScoreTable <- FALSE
  runSaveModelScore(isNightly, resetScoreTable)
  
  # run tests on DB result
  accountMessageSequence <- dbGetQuery(con, 'SELECT * from AccountMessageSequence;')
  expect_equal(dim(accountMessageSequence), c(368,9))
  messageRescoringTimes <- dbGetQuery(con_l, 'SELECT * from MessageRescoringTimes;')
  expect_equal(dim(messageRescoringTimes), c(106,7))
})

dbDisconnect(con)
dbDisconnect(con_l)
