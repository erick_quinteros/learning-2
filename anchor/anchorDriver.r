##########################################################
#
#
# aktana- anchor location estimates Aktana Learning Engines.
#
# description: estimate rep likelihood of being in a location
#
# created by : marc.cohen@aktana.com
# updated by : shirley.xu@aktana.com
#
# created on : 2016-11-01
# updated on: 2018-07-31
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
library(Learning)
library(Learning.DataAccessLayer)
library(futile.logger)
library(uuid)

############################################
## Main program
############################################
DRIVERMODULE <- "anchorDriver.r"
CONFIG_UID <- 'AKT_ANCHOR_V0'  # currently CONFIG_UID is fixed for anchor, may change later
options(rgl.useNULL=TRUE)

# read args from command passed from bash script
args <- commandArgs(TRUE)
if(length(args)==0){
  print("No arguments supplied.")
  quit(save = "no", status = 1, runLast = FALSE)
}else{
  print("Arguments supplied.")
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
    print(args[[i]]);
  }
}
# config port param to be compatibale to be passed into dbConnect
if (!exists('port')) {
  port <- 0
} else {
  port <- as.numeric(port)
}

# check if running in debugmode
if (!exists("debugMode")) {
  debugMode <- FALSE
} else {
  debugMode <- as.logical(debugMode)
  if (is.null(debugMode)) {
    debugMode <- FALSE # set to false to non logical convertible value
  }
}

# check whether isNightly run or not
isNightly <- F
if(!exists("BUILD_UID")) isNightly <- T    # determine if this is part of a nightlyrun

# get RUN_UID
if (isNightly) { RUN_UID <- UUIDgenerate() }

# replace cleanup function to save failure in learning DB
cleanUpAnchor <- function () 
{   
  tryCatch ({
    # update DBtable
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
    # update learningRun table to record failure
    dataAccessLayer.common.write.failureLearningRun(RUN_UID, now)
  },
  error = function(e){flog.error("Running Error Handler failed: %s", e)},
  finally = {
    # disconnet DB and exit
    dataAccessLayer.common.closeConnections()
    q('no',status=1)}
  )
  # exit()
}

# load library
source(sprintf("%s/anchor/code/loadAnchorData.r",homedir))
source(sprintf("%s/anchor/code/calculateAnchor.r",homedir))
source(sprintf("%s/anchor/code/calculateAccountDateLocation.r",homedir))
source(sprintf("%s/anchor/code/calculateAnchorForNewRep.r",homedir))
source(sprintf("%s/anchor/code/calculateCalendarAdherence.r",homedir))
source(sprintf("%s/anchor/code/processAccountDateLocationResult.r",homedir))
source(sprintf("%s/anchor/code/processAnchorResult.r",homedir))
source(sprintf("%s/anchor/code/saveAnchorResult.r",homedir))
source(sprintf("%s/anchor/code/saveAccountDateLocationResult.r",homedir))
source(sprintf("%s/anchor/code/saveCalendarAdherenceResult.r",homedir))
source(sprintf("%s/anchor/code/utils.r",homedir))

# read configs
if (isNightly) {
  config <- initializeConfigurationNew(homedir, CONFIG_UID, inNightlyFolder=TRUE)
} else {
  config <- initializeConfigurationNew(homedir, BUILD_UID)
}
BUILD_UID  <- config[["buildUID"]]
versionUID <- config[["versionUID"]]
configUID  <- config[["configUID"]]

# establish connections to learning data sources
dataAccessLayer.common.initializeConnections(dbuser, dbpassword, dbhost, dbname, port)

# initialize the client
runSettings <- initializeClientNew(DRIVERMODULE, homedir, BUILD_UID, RUN_UID, errorFunc=cleanUpAnchor, createExcel=FALSE, createModelsaveDir=FALSE, debugMode=debugMode)
# log start of running for nightly (manual running will have this from API)
if (isNightly) {
  now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
  dataAccessLayer.common.write.runningLearningRun(RUN_UID,BUILD_UID,versionUID,configUID,'ANCHOR',now)
}
flog.info("Run initialized at: %s",as.character(Sys.time()))

# find out predict rundate offset (for determining whether starting predict from today or tommorow based on the different anchor is running)
# default value
needPredictOffset <- as.integer(strftime(Sys.time(), format="%H")) > 16
# read from config if set
predictAdheadFrom <- getConfigurationValueNew(config,"LE_AN_predictAdheadFrom", convertFunc=as.character, defaultValue="auto")
needPredictOffset <- switch(predictAdheadFrom, auto=needPredictOffset, today=FALSE, tomorrow=TRUE, needPredictOffset)
# if manual run fixed rundateOffset = 1
needPredictOffset <- ifelse(!isNightly, TRUE, needPredictOffset)
flog.info("Running Anchor at %s, predictAdheadFrom=%s, needPredictOffset=%s", as.character(Sys.time()), predictAdheadFrom, needPredictOffset)

# read module specific configs
# find out predict rundate
if (isNightly) {
  predictRundate <- getConfigurationValueNew(config,"LE_AN_nightlyPredictRundate", convertFunc=as.Date, defaultValue=Sys.Date())
  flog.info('Running Anchor nightly: predictRundate=%s', as.character(predictRundate))
} else {  # manual scoring
  predictRundate <- getConfigurationValueNew(config,"LE_AN_manualPredictRundate", convertFunc=as.Date, defaultValue=Sys.Date())
  flog.info('Running Anchor manually: predictRundate=%s', as.character(predictRundate))
}

# adjust predictRundate if not needPredictOffset
if (!needPredictOffset) {
  predictRundate <- predictRundate - 1
  flog.info("Adjust predictRundate to %s based on predictAdheadFrom setting", as.character(predictRundate))
}

# determine whether needs to run accountDateLocationScores calcualtions or not
flog.info("Determine whether to run recalcualte AccountDateLocationScores")
adlScores <- data.table(dataAccessLayer.anchor.get.adlScores())
calculate_adlScores <- utils.determineIfRunWeeklyRecalculation(predictRundate, isNightly, adlScores, BUILD_UID)

# determine if run calendar adherence
calendarAdherenceOn <- getConfigurationValueNew(config,"LE_AN_useCalendarAdherence", convertFunc=as.logical, defaultValue=TRUE)
if (!calendarAdherenceOn) {
  flog.info("CalendarAdherence Off: not use & calcualte calendar adherence")
  calculate_calendarAdherence <- FALSE
} else {
  flog.info("CalendarAdherence On: determine if needs to run calendar adherence...")
  calAdherence <- data.table(dataAccessLayer.anchor.get.calAdherence())
  calculate_calendarAdherence <- utils.determineIfRunWeeklyRecalculation(predictRundate, isNightly, calAdherence, BUILD_UID)
}

# process other paramenters in learning.properties
numberCores <- getConfigurationValueNew(config,"LE_AN_numberCores", convertFunc=as.integer, defaultValue=4)
predictAhead <- getConfigurationValueNew(config,"LE_AN_predictAhead", convertFunc=as.integer, defaultValue=5) # predict how many days in the future
withinDayClusterRatio <- getConfigurationValueNew(config,"LE_AN_withinDayClusterRatio", convertFunc=as.integer, defaultValue=3) # in learning.properties now, but not exposed to user
historyWindow <- getConfigurationValueNew(config,"LE_AN_historyWindow", convertFunc=as.integer, defaultValue=365) # how many days in history to look back
startDate <- predictRundate - historyWindow
predictMode <- getConfigurationValueNew(config,"LE_AN_predictMode", convertFunc=as.character, defaultValue="Centroid")
maxFarPercentile <- getConfigurationValueNew(config,"LE_AN_maxFarPercentile", convertFunc=as.numeric, defaultValue=0.75)
minIntsRequire <- getConfigurationValueNew(config,"LE_AN_minIntsRequire", convertFunc=as.integer, defaultValue=40)
fixOutput <- getConfigurationValueNew(config,"LE_AN_fixRandomSeed", convertFunc=as.logical, defaultValue=FALSE)
calAdprobThreshold <- getConfigurationValueNew(config,"LE_AN_calAdprobThreshold", convertFunc=as.numeric, defaultValue=0)
calAdconfThreshold <- getConfigurationValueNew(config,"LE_AN_calAdconfThreshold", convertFunc=as.numeric, defaultValue=0)

if(fixOutput) { set.seed(1) }

# get a list of days that will be predicted (skip weekends) (funcs in utils)
predictAheadDayList <- utils.generatePredictAheadDayList(predictRundate, predictAhead)
flog.info("PredictAhead=%s: predict for %s", predictAhead, paste(predictAheadDayList,collapse=", "))
# load the needed data
dta <- loadAnchorData(predictRundate, startDate, predictAheadDayList, calculate_adlScores, calendarAdherenceOn, calculate_calendarAdherence)
INTERACTIONS_REP <- dta[["INTERACTIONS_REP"]]
future <- dta[["future"]]
INTERACTIONS_ACCOUNT <- dta[["INTERACTIONS_ACCOUNT"]]
accountDateLocationScores <- dta[["accountDateLocationScores"]]
repAccountAssignments <- dta[["repAccountAssignments"]]
repTeamRep <- dta[["repTeamRep"]]
newRepList <- dta[["newRepList"]]
repCalendarAdherence <- dta[["repCalendarAdherence"]]
repAccountCalendarAdherence <- dta[["repAccountCalendarAdherence"]]
callHistory <- dta[["callHistory"]]

rm(dta)

# run calcualte calendar adherence
if (calculate_calendarAdherence) {
  if (nrow(callHistory)>0) {
    tmp <- calculateCalendarAdherence(callHistory)
    repCalendarAdherence <- tmp[["repCalendarAdherence"]]
    repAccountCalendarAdherence <- tmp[["repAccountCalendarAdherence"]]
    rm(tmp)
  } else {
    flog.warn("callHistory needed for calculating calendar adherence is empty")
    repCalendarAdherence <- data.table()
    repAccountCalendarAdherence <- data.table()
  }
}

# run the estimates for anchor
if (nrow(INTERACTIONS_REP)>0) {
  result <- calculateAnchor(INTERACTIONS_REP, future, predictRundate, predictAheadDayList, numberCores, withinDayClusterRatio, maxFarPercentile, minIntsRequire, calendarAdherenceOn, repCalendarAdherence, repAccountCalendarAdherence, calAdprobThreshold, calAdconfThreshold)
  newRepListAdd <- result[["newRepList"]]
  result <- result[["result"]]
} else {
  flog.warn("Interactions data needed for running Anchor is empty")
  newRepListAdd <- data.table()
  result <- data.table()
}


# run estimate for accountDateLocation
if(calculate_adlScores) {
  if (nrow(INTERACTIONS_ACCOUNT)>0) {
    accountDateLocationScores <- calculateAccountDateLocationScores(INTERACTIONS_ACCOUNT)
  } else {
    flog.warn("Interactions data needed for running calculateAccountDateLocationScores is empty")
    accountDateLocationScores <- data.table()
  }
}
# always compute and write to either dse or second learning table depending on isNightly
if (nrow(accountDateLocationScores)>0) {
  accountDateLocation <- calculateAccountDateLocation(accountDateLocationScores, fixOutput)
} else {
  flog.warn("accountDateLocationScores needed for calculateAccountDateLocation is empty")
  accountDateLocation <- data.table()
}


# estimate for new rep
newRepList <- rbind(newRepList, newRepListAdd)
if (nrow(newRepList)>0) {
  # do terriotry change estimate
  result_newRep <- calculateAnchorForNewRep(newRepList, future, repAccountAssignments, repTeamRep, accountDateLocation, result, numberCores, predictRundate, predictAheadDayList, calendarAdherenceOn, repCalendarAdherence, repAccountCalendarAdherence, calAdprobThreshold, calAdconfThreshold, fixOutput)
  if (nrow(result_newRep)>0) {
    result <- rbind(result, result_newRep)
  } else {
    flog.warn("calcualted result_newRep is empty")
  }
} else {
  flog.info("No new rep or rep missing prediction using clustering method in the data")
}

# save the anchor results
if (nrow(result) > 0) {
  resultList <- processAnchorResult(predictMode, result, predictRundate, isNightly)
  saveAnchorResult(isNightly, resultList, RUN_UID, BUILD_UID)
} else {
  flog.error("calculated Anchor result is empty!")
	stop("calculated Anchor result is empty!")
}

# save the accountDateLocation results
if(calculate_adlScores) {
  if (nrow(accountDateLocationScores) > 0) {
    saveAccountDateLocationScores(accountDateLocationScores, RUN_UID, BUILD_UID, predictRundate)
  } else {
    flog.error("calculated accountDateLocationScores result is empty!")
    stop("calculated accountDateLocationScores result is empty!")
  } 
}
if(nrow(accountDateLocation) > 0) {
  accountDateLocation <- processAccountDateLocationResult(accountDateLocation, predictAheadDayList, predictRundate)
  saveAccountDateLocation(accountDateLocation, RUN_UID, BUILD_UID, isNightly)
} else {
  flog.error("calculated accountDateLocation result is empty!")
  stop("calculated accountDateLocation result is empty!")
}

# save calendar adherence result if recalculated
if(calculate_calendarAdherence) {
  if (nrow(repCalendarAdherence) > 0) {
    saveRepCalendarAdherence(repCalendarAdherence, RUN_UID, BUILD_UID, predictRundate)
  } else {
    flog.warn("calculated repCalendarAdherence result is empty!")
  } 
  if (nrow(repAccountCalendarAdherence) > 0) {
    saveRepAccountCalendarAdherence(repAccountCalendarAdherence, RUN_UID, BUILD_UID, predictRundate)
  } else {
    flog.warn("calculated repAccountCalendarAdherence result is empty!")
  } 
}


# update entry in LearningDB (LearningRun)
now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")

tryCatch(dataAccessLayer.common.write.successLearningRun(RUN_UID, now), 
         error = function(e) {
           flog.error('Error in update LearningRun: %s', dbnameLearning, name='error')
         }) 

# db disconnect
flog.info("Finished running Anchor model")

dataAccessLayer.common.closeConnections()

# final clean up
closeClient()



