context('testing saveRepCalendarAdherence, saveRepAccountCalendarAdherence in ANCHOR module')
print(Sys.time())

# load library and source script
library(data.table)
library(Learning.DataAccessLayer)
source(sprintf("%s/anchor/code/saveCalendarAdherenceResult.r",homedir))

# set up DB connection required for function
dataAccessLayer.common.initializeConnections(dbuser, dbpassword, dbhost, dbname, port)

# test saveAccountDateLocationScores
test_that("test saveRepCalendarAdherence", {
  # run saveAccountDateLocationScores
  RUN_UID <- "RUN_UID"
  BUILD_UID <- "BUILD_UID"
  predictRundate <- as.Date(readModuleConfig(homedir, 'anchor','rundate'))
  load(sprintf('%s/anchor/tests/data/from_calculateCalendarAdherence_repCalendarAdherence.RData',homedir))
  requiredMockDataList <- list(pfizerusdev_learning=c('RepCalendarAdherence'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)
  saveRepCalendarAdherence(repCalendarAdherence, RUN_UID, BUILD_UID, predictRundate) 
  
  # run tests on DB result
  repCalendarAdherence_new <- data.table(dbGetQuery(con_l, "SELECT * FROM RepCalendarAdherence;"))
  # test cases
  # test dimension
  expect_equal(dim(repCalendarAdherence_new), c(2,10))
  # test entry in accountDateLocationScores
  expect_equal(repCalendarAdherence_new[order(repId),names(repCalendarAdherence),with=F], repCalendarAdherence[order(repId),])
  expect_equal(unique(repCalendarAdherence_new$learningRunUID),RUN_UID)
  expect_equal(unique(repCalendarAdherence_new$learningBuildUID),BUILD_UID)
  expect_equal(unique(repCalendarAdherence_new$runDate),as.character(predictRundate))
})

test_that("test saveRepAccountCalendarAdherence", {
  # run saveAccountDateLocationScores
  RUN_UID <- "RUN_UID"
  BUILD_UID <- "BUILD_UID"
  predictRundate <- as.Date(readModuleConfig(homedir, 'anchor','rundate'))
  load(sprintf('%s/anchor/tests/data/from_calculateCalendarAdherence_repAccountCalendarAdherence.RData',homedir))
  requiredMockDataList <- list(pfizerusdev_learning=c('RepAccountCalendarAdherence'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)
  saveRepAccountCalendarAdherence(repAccountCalendarAdherence, RUN_UID, BUILD_UID, predictRundate) 
  
  # run tests on DB result
  repAccountCalendarAdherence_new <- data.table(dbGetQuery(con_l, "SELECT * FROM RepAccountCalendarAdherence;"))
  # test cases
  # test dimension
  expect_equal(dim(repAccountCalendarAdherence_new), c(8,11))
  # test entry in accountDateLocationScores
  expect_equal(repAccountCalendarAdherence_new[order(repId, accountId),names(repAccountCalendarAdherence),with=F], repAccountCalendarAdherence[order(repId, accountId),])
  expect_equal(unique(repAccountCalendarAdherence_new$learningRunUID),RUN_UID)
  expect_equal(unique(repAccountCalendarAdherence_new$learningBuildUID),BUILD_UID)
  expect_equal(unique(repAccountCalendarAdherence_new$runDate),as.character(predictRundate))
})
