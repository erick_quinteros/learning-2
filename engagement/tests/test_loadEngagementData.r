context('testing loadEngagementData() func in REM module')
print(Sys.time())

# load library and source script
library(RMySQL)
library(properties)
library(futile.logger)
source(sprintf("%s/engagement/code/loadEngagementData.R",homedir))
# required to run tests
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))

# writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
requiredMockDataList <- list() # use module own data
requiredMockDataList_module <- list(pfizerusdev=c('Interaction','InteractionAccount','Account','Rep','RepAccountAssignment'),pfizerusdev_stage=c('Suggestion_Feedback_vod__c_arc','RecordType_vod__c_arc','Suggestion_vod__c_arc'))
resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList,requiredMockDataList_module,'engagement')

# set up unit test build folder and related configs
# get buildUID
BUILD_UID <- readModuleConfig(homedir, 'engagement','buildUID')
# add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
setupMockBuildDir(homedir, 'engagement', BUILD_UID, files_to_copy='learning.properties')

# parameters needed to run func
propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,BUILD_UID)
config <- read.properties(propertiesFilePath)
startHorizon <- as.Date(config[["LE_RE_startHorizon"]])  # make the startHorizon a date
useForProbability <- config[["LE_RE_useForProbability"]] # parsing of useForProbability
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)

# run script
loadEngagementData(con, dbname, startHorizon, useForProbability)
dbDisconnect(con)

# test case
test_that("test have correct length of data", {
  expect_equal(dim(interactions),c(218,5))
  expect_equal(dim(suggestions),c(25,9))
})

test_that("test result is the same as the one saved ", {
  load(sprintf('%s/engagement/tests/data/from_loadEngagementData.RData', homedir))
  expect_equal(interactions,data[[1]])
  expect_equal(suggestions,data[[2]])
})