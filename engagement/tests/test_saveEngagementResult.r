context('testing saveEngagementResult() func in REM module')
print(Sys.time())

# load library and source script
library(properties)
library(futile.logger)
library(uuid)
library(RMySQL)
library(data.table)
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf("%s/engagement/code/saveEngagementResult.R",homedir))

# writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
requiredMockDataList <- list(pfizerusdev=c('RepAccountEngagement','RepTeam'),pfizerusdev_learning=c('RepAccountEngagement'))
requiredMockDataList_module <- list(pfizerusdev=c('Account','Rep','RepTeamRep'))
resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList,requiredMockDataList_module,'engagement')

# set up unit test build folder and related configs
# get buildUID
BUILD_UID <- readModuleConfig(homedir, 'engagement','buildUID')
# add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
setupMockBuildDir(homedir, 'engagement', BUILD_UID, files_to_copy='learning.properties')

# parameters needed to run func
if(!exists("RUN_UID")) RUN_UID <- UUIDgenerate()
# parameters from config
propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,BUILD_UID)
config <- read.properties(propertiesFilePath)
versionUID <- config[["versionUID"]]
configUID  <- config[["configUID"]]
today <<- min(as.Date(config[["LE_RE_today"]]), Sys.Date())-1
# db connection
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)

# run func and test cases
test_that('test for save for manual running', {
  # run saveModelScore()
  isNightly <- FALSE
  RUN_UID <- UUIDgenerate()
  load(sprintf('%s/engagement/tests/data/from_calculateEngagement.RData', homedir))
  result$learningRunUID <- RUN_UID
  saveEngagementResult(con, con_l, result, BUILD_UID, RUN_UID, configUID, versionUID, isNightly)
  
  # run tests on DB result
  repAccountEngagement <- dbGetQuery(con, 'SELECT * from RepAccountEngagement;')
  repAccountEngagement_l <- dbGetQuery(con_l, 'SELECT * from RepAccountEngagement;')
  # test cases
  expect_equal(dim(repAccountEngagement), c(0,10))
  expect_equal(dim(repAccountEngagement_l), c(15,10))
  
  # run saveEngagementResult() again whithout clear DB to ensure if recording with the same buildUID already exists in DB, it would not delete old scores and add new scores
  RUN_UID <- UUIDgenerate()
  load(sprintf('%s/engagement/tests/data/from_calculateEngagement.RData', homedir))
  result$learningRunUID <- RUN_UID
  SQL <- "INSERT INTO RepAccountEngagement VALUES('test','test',3,1505,3357,'Target','2018-07-20',0,'2018-07-20','2018-07-20');"
  dbClearResult(dbSendQuery(con_l, SQL))
  saveEngagementResult(con, con_l, result, BUILD_UID, RUN_UID, configUID, versionUID, isNightly)
  
  # run tests on DB result
  repAccountEngagement_l <- dbGetQuery(con_l, 'SELECT * from RepAccountEngagement;')
  # test cases
  expect_equal(dim(repAccountEngagement_l), c(16,10))
  expect_setequal(unique(repAccountEngagement_l$learningRunUID),c('test',RUN_UID))
})

test_that('test for save for nightly running', {
  # run saveModelScore()
  isNightly <- TRUE
  RUN_UID <- UUIDgenerate()
  load(sprintf('%s/engagement/tests/data/from_calculateEngagement.RData', homedir))
  result$learningRunUID <- RUN_UID
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)
  saveEngagementResult(con, con_l, result, BUILD_UID, RUN_UID, configUID, versionUID, isNightly)
  
  # run tests on DB result
  repAccountEngagement <- dbGetQuery(con, 'SELECT * from RepAccountEngagement;')
  repAccountEngagement_l <- dbGetQuery(con_l, 'SELECT * from RepAccountEngagement;')
  # test cases
  expect_equal(dim(repAccountEngagement), c(15,10))
  expect_equal(dim(repAccountEngagement_l), c(0,10))
  
  # run saveEngagementResult() again whithout clear DB to ensure if recording with the same buildUID already exists in DB, it would not delete old scores and add new scores
  RUN_UID <- UUIDgenerate()
  load(sprintf('%s/engagement/tests/data/from_calculateEngagement.RData', homedir))
  result$learningRunUID <- RUN_UID
  SQL <- "INSERT INTO RepAccountEngagement VALUES(3,1505,3357,'Target','test','2018-07-20',0,'2018-07-19','2018-07-19','2018-07-19');"
  dbClearResult(dbSendQuery(con, SQL))
  saveEngagementResult(con, con_l, result, BUILD_UID, RUN_UID, configUID, versionUID, isNightly)

  # run tests on DB result
  repAccountEngagement <- dbGetQuery(con, 'SELECT * from RepAccountEngagement;')
  # test cases
  expect_equal(dim(repAccountEngagement), c(15,10))
  expect_equal(repAccountEngagement$learningRunUID[1],RUN_UID)
})

# disconnect DB
dbDisconnect(con)
dbDisconnect(con_l)