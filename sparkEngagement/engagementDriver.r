##########################################################
#
#
# aktana- engagement estimates estimates Aktana Learning Engines.
#
# description: estimate rep likelihood of engaging
#
# created by : marc.cohen@aktana.com
# updated by : wendong.zhu@aktana.com
#
# updated on : 2019-03-12
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
library(RMySQL)
library(data.table)
library(properties)
library(uuid)
library(futile.logger)
library(Learning)
library(sparklyr)
library(sparkLearning)
library(arrow)

#################################################
## function: cleanUp (errorhandler function)
#################################################
cleanUp <- function () 
{   
  tryCatch ({
    # 
    if (!exists(isNightly)) isNightly <- T
    
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
    # update learningRun table for manual scoring job
    if(!isNightly) {
      SQL <- sprintf("UPDATE LearningRun SET executionDateTime='%s', executionStatus='failure' WHERE learningRunUID='%s';",now,RUN_UID) 
      dbGetQuery(con_l, SQL)
    } else {
      SQL <- sprintf("INSERT INTO LearningRun(learningRunUID,learningBuildUID,learningVersionUID,learningConfigUID,isPublished,runType,executionStatus,executionDateTime) VALUES('%s','%s','%s','%s',1,'REM','failure','%s');", RUN_UID,BUILD_UID,VERSION_UID,CONFIG_UID,now)
      dbGetQuery(con_l, SQL)
    }
  },
  error = function(e){flog.error("Running Error Handler failed: %s", e)},
  finally = {
    # disconnet DB and exit
    dbDisconnect(con)
    dbDisconnect(con_l)
    q('no',status=1)}
  )
  # exit()
}

############################################
## Main program
############################################
if(F)  # this is just for testing locally
{
    # parameter defaults
    lookForward <- 3
    epsilon <- .0001
    today <- Sys.Date()
    numberCores <- 2
    channels <- c(3,8,12,13)
    startHorizon <- "2016-01-01"
    sugStartDate <- 90
    includeReactions <- c("Complete","Execution")
    useForProbability <- "B"
    ###########
}

########################################
# currently CONFIG_UID is fixed for REM
# it may change later
########################################
CONFIG_UID <- 'AKT_REM_V0' 

options(rgl.useNULL=TRUE)

# set parameters 
args <- commandArgs(TRUE)

if(length(args)==0){
    print("No arguments supplied.")
    quit(save = "no", status = 1, runLast = FALSE)
}else{
    print("Arguments supplied.")
    for(i in 1:length(args)){
      eval(parse(text=args[[i]]))
      print(args[[i]]);
    }
}

# config port param to be compatibale to be passed into dbConnect
if (!exists('port')) {
  port <- 0
} else {
  port <- as.numeric(port)
}

DRIVERMODULE <- "engagementDriver.r"

source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf("%s/sparkEngagement/code/calculateEngagement.R",homedir))
source(sprintf("%s/sparkEngagement/code/calculateTriggerEngagement.R",homedir))
source(sprintf("%s/sparkEngagement/code/loadEngagementData.R",homedir))
source(sprintf("%s/sparkEngagement/code/saveEngagementResult.R",homedir))
source(sprintf("%s/sparkEngagement/code/utils.R",homedir))

# establish db connection to learning DB (con_l) and DSE DB (con)
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
# spark db connections config
sparkDBconURL <- sparkGetDBConnection(dbuser, dbpassword, dbhost, dbname, port)
sparkDBconURL_l <- sparkGetDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
sparkDBconURL_stage <- sparkGetDBConnectionStage(dbuser, dbpassword, dbhost, dbname, port)

# get spark connection
if (!exists("sc")) {
  sc <- initializeSpark(homedir, master="yarn-client", version="2.3.1")
}

isNightly <- F
if(!exists("BUILD_UID"))isNightly <- T    # determine if this is part of a nightlyrun

if (isNightly)
{
    propertiesFilePath <- sprintf("%s/builds/nightly/%s/learning.properties", homedir, CONFIG_UID)    
    if (file.exists(propertiesFilePath)) {
        config <- read.properties(propertiesFilePath)
    }
    else {
        print("The learning.properties for this config %s doesn't exist!", propertiesFilePath)    
        quit(save = "no", status = 1, runLast = FALSE) 
    }
    # generate a unique runUID
    RUN_UID <- UUIDgenerate()
}    

if (!isNightly)
{
    propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,BUILD_UID)
    if (file.exists(propertiesFilePath)) {
        config <- read.properties(propertiesFilePath)
    }
    else {
        print("The learning.properties for this config %s doesn't exist!", propertiesFilePath)  
        quit(save = "no", status = 1, runLast = FALSE)   
    }    
}

BUILD_UID  <- config[["buildUID"]]
versionUID <- config[["versionUID"]]
configUID  <- config[["configUID"]]

# initialize the client
runSettings <- initializeClientNew(DRIVERMODULE, homedir, BUILD_UID, RUN_UID, errorFunc=cleanUp)
flog.info("Run initialized at: %s",Sys.time())

# insert parameters read from sysParameters table
names <- gsub("LE_RE_","",names(config))  # remove the LE_RE_ prefix 
for(i in 1:(length(names)))assign(names[i],config[i])  # turn each of the configuration parameters into a variable

# this is some cleanup of the config paramenters
startHorizon <- as.Date(unlist(startHorizon))  # make the startHorizon a date
today <- min(as.Date(unlist(today)), Sys.Date())-1
sugStartDate <- today-as.numeric(sugStartDate) # how far back from today to read the data
lookForward <- max(1, as.numeric(lookForward)) # how far forward from today to predict
engageWindow <- max(lookForward, as.numeric(EngageWindow))  # how far forward to look for engagements for the suggestions that are ignored
epsilon <- as.numeric(epsilon)                 # for numerical equivalence to zero
numberCores <- as.numeric(numberCores)         # number of cores to use
channels <- strsplit(unlist(channels),split=";")[[1]]  # minor parsing of the channel parameter
includeReactions <- strsplit(unlist(includeReactions), split=";")[[1]]  # minor parsing of which reactions to include
useForProbability <- unlist(useForProbability) # parsing of useForProbability

flog.info("useForProbability is %s", useForProbability)

dta <- loadEngagementData(sc, sparkDBconURL, sparkDBconURL_stage, startHorizon, useForProbability)  # call the data read function

for(i in 1:length(dta)) {
    assign(names(dta)[i], dta[[i]])
}

rm(dta)

# only include channels that have suggestions
#channels <- channels[channels %in% unique(suggestions$repActionTypeId)]
repActionTypeId.unique <- unique(pull(suggestions, repActionTypeId))
channels <- channels[channels %in% repActionTypeId.unique]

# loop through the estimation code for each channel and collect results
if (length(channels) > 0) {
  result <- NULL
  for(i in 1:length(channels))
  {
    # this is estimation for the target driven stuff
    result <- sdf_bind_rows(calculateEngagement(sc, channels[i], useForProbability, interactions, suggestions), result)
    
    # this is estimation for the trigger driven stuff
    result <- sdf_bind_rows(calculateTriggerEngagement(sc, channels[i], useForProbability, interactions, suggestions), result)   
  } 

  # save the results in the RepAccountEngagement table
  saveEngagementResult(sc, sparkDBconURL, sparkDBconURL_l, con, con_l, result, BUILD_UID, RUN_UID, configUID, versionUID, isNightly)

} else {
  flog.warn("no data avaible in all channels specified!")
}

# update running status, executionDateTime. 
now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
if (isNightly) {
  # insert into the learning database with the run
  SQL <- sprintf("INSERT INTO LearningRun(learningRunUID,learningBuildUID,learningVersionUID,learningConfigUID,isPublished,runType,executionStatus,executionDateTime) VALUES('%s','%s','%s','%s',1,'REM','success','%s');",RUN_UID,BUILD_UID,versionUID,configUID,now)
  flog.info("INSERT INTO LearningRun SQL")
  dbGetQuery(con_l, SQL)
} else {
  SQL <- sprintf("UPDATE LearningRun SET executionDateTime='%s', executionStatus='success' WHERE learningRunUID='%s';",now,RUN_UID) 
  tryCatch(dbGetQuery(con_l, SQL), 
           error = function(e) {
             flog.error('Error in update LearningRun: %s', dbnameLearning, name='error')
           }) 
}

# Disconnet DB and release handles
dbDisconnect(con)
dbDisconnect(con_l)
closeSpark(sc)

# final clean up
closeClient()
