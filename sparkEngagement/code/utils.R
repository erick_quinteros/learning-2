####################################################################################################################
#
#
# aktana- engagement estimates Aktana Learning Engines.
#
# description: helper functions.  
#
# created by : wendong.zhu@aktana.com
#
# created on : 2018-10-26
#
# Copyright AKTANA (c) 2018.
#
#
####################################################################################################################

library(data.table)
library(sparklyr)
library(dplyr)
library(futile.logger)

# Function to caldulate ratio for each weekday (with weak of month) in order to
#    allocate probabilities
#
# Input: dayEstimate - dataframe or data.table of probabilities
#        today - today's date
#        loofForward - predicted number of days for future
# Output: dayEstimate
#
calculateDaysEstimate <- function(dayEstimate, today, lookForward)
{
    #setkey(dayEstimate,accountId,repId,date)   # the estimates are obtained by simple counting for each rep-account and then taking the ratio - multinomial approach

    #dayEstimate <- unique(dayEstimate, by=c("accountId", "repId", "date"))
    dayEstimate <- dayEstimate %>% distinct(accountId, repId, date) # distinct will drop the other columns, but since the other columns are not used, so it is fine to drop them

    #dayEstimate[, c("monthday", "weekday") := list(as.numeric(format(date,"%d")), as.factor(weekdays(date)))]
    #dayEstimate <- dayEstimate %>% mutate(monthday = month(date), weekday = weekdays(date))
    dayEstimate <- dayEstimate %>% mutate(monthday = date_format(date, "d"), weekday = date_format(date, "E"))
    
    #dayEstimate[, monthweek := ceiling(monthday/7)]
    dayEstimate <- dayEstimate %>% mutate(monthweek = ceiling(monthday/7))

    #dayEstimate[, mw.wd := paste0(monthweek, "_", weekday)]
    #dayEstimate <- dayEstimate %>% mutate(mw.wd = paste(monthweek, "_", weekday))
    dayEstimate <- dayEstimate %>% mutate(mw_wd = concat_ws("_", monthweek, weekday))

    #dayEstimate <- dayEstimate[, c("repId", "accountId", "mw.wd"), with=F]
    dayEstimate <- dayEstimate %>% select(repId, accountId, mw_wd)

    #dayEstimate$ctr <- 1
    dayEstimate <- dayEstimate %>% mutate(ctr = 1)

    #dayEstimate[, tot := sum(ctr), by = c("repId", "accountId", "mw.wd")]
    dayEstimate <- dayEstimate %>% group_by(repId, accountId, mw_wd) %>% 
      mutate(tot = sum(ctr,na.rm=TRUE)) %>% ungroup() 

    #setkey(dayEstimate, repId, accountId, mw.wd)
    dayEstimate <- arrange(dayEstimate, repId, accountId, mw_wd)

    days <- paste0(ceiling(as.numeric(format(today+(1:lookForward), "%d"))/7), "_", weekdays(today+(1:lookForward), abbreviate = T))

    #dayEstimate <- dayEstimate[mw.wd %in% days]
    #dayEstimate <- unique(dayEstimate)
    dayEstimate <- dayEstimate %>% filter(mw_wd %in% days) %>% distinct()

    #dayEstimate[, total := sum(tot), by=c("repId","accountId")]
    dayEstimate <- dayEstimate %>% group_by(repId, accountId) %>% 
      mutate(total = sum(tot,na.rm=TRUE)) %>% ungroup() 

    #dayEstimate[, ratio := tot/total]             # the estimates are saved in the dayEstimate table
    dayEstimate <- dayEstimate %>% mutate(ratio = tot/total)

    return (dayEstimate)
}

# Function to add weekday and monthday to probability dataframe
#
# Input: probs - dataframe or data.table of probabilities
#        today - today's date
#        loofForward - predicted number of days for future
# Output: probs
#
addWeekdayWeekmonth <- function(probs, today, lookForward)
{
    tmp <- NULL
    for(dte in 1:lookForward)         # prepare to spread the estimates over the lookForward interval
    {
        #probs$date <- today + dte     # replicate the estimate for each lookForward day
        probs <- probs %>% mutate(date = date_add(today, dte))
    
        #tmp <- rbind(probs, tmp)
        tmp <- sdf_bind_rows(probs, tmp)
    }
    probs <- tmp
    
    # use the day of week & week of month estimates for distributing weights over the next lookForward days
    probs <- probs %>% mutate(monthday = date_format(date, "d"), weekday = date_format(date, "E"))
    
    #probs[, monthweek := ceiling(monthday/7)]
    #probs[, mw.wd := paste0(monthweek,"_",weekday)]
    probs <- probs %>% mutate(monthweek = ceiling(monthday/7), mw_wd = concat_ws("_", monthweek, weekday))
    
    return (probs)
}

# Function to caldulate the probability that an intereaction will happen 
# between the the time of the last interaction and the lookForward days
#
# Input: events - dataframe or data.table of probabilities
#        acctReps
#        today - today's date
#        numberCores
# Output: likelihood
#

# helper function to estimate the likelihood for a visit based on the ECDF function fn
E <- function(fn,x) return(fn$estimate[which(abs(fn$eval.points-x)==min(abs(fn$eval.points-x)))])

calculateLikelihood <- function(events, acctReps, today, numberCores)
{
    library(ks)
    library(foreach)
    library(doMC)

    flog.info("size of events: (%s)", paste(sdf_dim(events), collapse=','))
    flog.info("size of acctReps: (%s)", paste(sdf_dim(acctReps), collapse=','))
  
    events <- data.table(collect(events))
    flog.info("finish collect events")
    acctReps <- data.table(collect(acctReps))
    flog.info("finish collect acctReps")

    M <- dim(acctReps)[1]                                                            # setup for parallel processing
    N <- data.table(rowIndex=1:M,group=1:numberCores)    # grouping for each core

    flog.info("calculating of likelihood in parallel, using %s cores", numberCores)

    likelihood <- foreach(nc = 1:numberCores , .combine=rbind) %dopar%               # loop for each core
    {
      inds <- N[group==nc]$rowIndex               # index of rows assigned to be run by this core 
      indsLen <- length(inds)  
      
      likelihood <- data.table(repId=numeric(indsLen),accountId=numeric(indsLen),probTouch=numeric(indsLen))  # for collection of results

        for(i in 1:indsLen)
        {
            ind <- inds[i] 
            ydata <- events[k==acctReps[ind]$k]                                        # pick out data for a single rep-account combination

            if(nrow(ydata)>2)                                                        # if there are less than 3 data points then skip - not enough data for estimation
            {
                prob <- 0
                lastDiff <- today-ydata[,.SD[.N]]$date                               # find the time between the last interaction and today
                maxDiff <- max(ydata$diff,na.rm=T)                                   # find the maximum time between interactions
                if(lastDiff<=maxDiff)                                                # only continue if the last difference is less than the max
                {
                    fn <- kcde(ydata[!is.na(diff)]$diff,h=1)                         # estimate the ECDF using the kcde() function with smoothing parameter h=1
                    prob <- (1 - E(fn,lastDiff)) - (1 - E(fn,lastDiff+lookForward))  # estimate the probability that an intereaction will happen between the the time of the last one and the lookForward days
                }

                loc <- i
                set(likelihood,loc,1,acctReps[ind,"repId",with=F])                     # save the results in the likelihood table that was setup above
                set(likelihood,loc,2,acctReps[ind,"accountId",with=F])
                set(likelihood,loc,3,prob)
            }
        }

        flog.info("Done with calculating probTouch using events/interactions. Now estimates the likelihood of an interaction by day of week and week of month.")

        return(likelihood)                                                           # return here is for the parallel processing loop
    }
}
